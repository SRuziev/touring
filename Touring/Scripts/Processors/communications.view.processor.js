﻿"use strict";
class СommunicationsViewProcessor {
    constructor(config) {
        this._config = config;
    }

    // private
    FindByName(name) {
        return $(`[let="${name}"]`);
    }

    TryValue(object) {
        if (object == undefined || object === NaN || object.length === 0)
            return "";

        return object.val();
    }

    // private
    BindClick(name, callback) {
        this.FindByName(name).unbind("click").on("click", callback);
    }

    // private
    ShowModal(title, html, callback) {
        var config = this._config;
        bootbox.dialog({
            size: "large",
            title: title,
            message: html,
            backdrop: true,
            animate: true,
            onEscape: true,
            buttons: {
                save: {
                    label: `<i class="fa fa-check"></i> ${config.resource.save}`,
                    className: "btn btn-sm btn-success text-uppercase",
                    callback: callback
                },
                cancel: {
                    label: `<i class="fa fa-ban"></i> ${config.resource.close}`,
                    className: "btn btn-sm btn-danger text-uppercase",
                    callback: function () {
                        bootbox.hideAll();
                    }
                }
            }
        });
    }


    HideModelErrors() {
        $("[valid-for]").html("");
    }

    ShowModelErrors(errors, formName) {
        var form = this.FindByName(formName);
        $.each(errors, function (key) {
            form.find(`[valid-for='${key}']`).html(errors[key]);
        });
    }

    Bindings() {
        var processor = this;
        this.BindClick("add-communications", function () { processor.AddNew() });
    }


    AddNew() {
        var processor = this;
        var config = this._config;

        let email = this.FindByName("email").val();

        MaskHelper.ShowBlockUi();
        $.get(config.url.add, { email: email }).done((data) => {
            MaskHelper.HideBlockUI();
            processor.ShowModal(config.resource.addTitle, data.html, function () {
                processor.AddNewSubmit(this);
                return false;
            });
        });
    }

    AddNewSubmit(btn) {
        $(btn).find("i").attr("class", "fa fa-spin fa-spinner");
        var processor = this;
        this.HideModelErrors();
        let model = this.FindByName("add-communications-form-add2").serialize();
        console.log(model);
        MaskHelper.ShowBlockUi();
        $.ajax({ url: this._config.url.add2, type: "POST", data: model }).done((data) => {
            $(btn).find("i").attr("class", "fa fa-check");
            MaskHelper.HideBlockUI();
            if (data.status) {
                bootbox.hideAll();
                CommonHelper.attention(processor._config.resource.operationComplete, processor._config.resource.addIsComplete);
            } else {
                processor.ShowModelErrors(data.errors, "add-communications-form-add2");
            }
        });
    }


    Start() {
        this.Bindings();
    }
}