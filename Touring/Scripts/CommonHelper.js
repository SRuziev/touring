﻿UIHelper = {
    ShowBlock: function (block) {
        block.removeClass('hide');        
    },
    HideBlock: function (block) {        
        block.addClass('hide');
    }
}

CommonHelper = new function () {
    this.executeFunctionByName = function (functionName, context /*, args */) {
        var args = [].slice.call(arguments).splice(2);
        var namespaces = functionName.split(".");
        var func = namespaces.pop();
        for(var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }
        return context[func].apply(context, args);
    };

    this.addParamToUrl = function(url, param, value) {
        var a = document.createElement('a'), regex = /(?:\?|&amp;|&)+([^=]+)(?:=([^&]*))*/gi;
        var params = {}, match, str = [];
        a.href = url;
        while (match = regex.exec(a.search))
            if (encodeURIComponent(param) != match[1])
                str.push(match[1] + (match[2] ? "=" + match[2] : ""));
        str.push(encodeURIComponent(param) + (value ? "=" + encodeURIComponent(value) : ""));
        a.search = str.join("&");
        return a.href;
    };
    this.GetFormOfject = function (formId) {
        var formData = {};
        var arrayParams = $('#' + formId).serializeArray();
        for (var i = 0; i < arrayParams.length; i++) {
            formData[arrayParams[i].name] = arrayParams[i].value;
        }
        return formData;
    };
    this.alert = function (title, msg, customCssClass) {
        bootbox.alert({
            title: title,
            message: msg,
            backdrop: true,
            animate: true,
            className: customCssClass
        });
    };
    
    this.attention = function (title, msg) {
        $.gritter.add({
            title: title,
            text: msg
        });
        return false;
    };
    
    this.warn = function (msg, title) {
        bootbox.dialog({
            title: title || "Внимание",
            message: msg,
            closeButton: true,
            animate: true,
            className: "warning-modal",
            buttons: {
                success: {
                    label: "OK"
                }
            }
        });
    };  
    this.confirm = function (title, textSuccess, textCancel) {
        bootbox.confirm({
            message: title,
            buttons: {
                confirm: {
                    label: '<i class="fa fa-check"></i> ' + textSuccess,
                    className: 'text-uppercase btn-success'
                },
                cancel: {
                    label: '<i class="fa fa-times"></i> ' + textCancel,
                    className: 'text-uppercase btn-default'
                }
            },
            callback: function (result) {
                return result;
                //console.log('This was logged in the callback: ' + result);
            }
        });
        //bootbox.confirm(title, function (result) {
        //    return result;
        //});        
    };
    this.Exception = function (model) {
        model.Title = model.Title == undefined ? 'Ошибка' : model.Title;
        model.Message = model.Message == undefined ? 'Неопознанная ошибка' : model.Message;
        model.ErrorDetails = model.ErrorDetails == undefined ? '' : model.ErrorDetails;

        bootbox.dialog({
            message: model.Message,
            title: model.Title,
            closeButton: true,
            animate: true,
            className: "error-modal",
            buttons: {
                success: {
                    label: "OK"
                }
            }
        });

    };
    this.getUrl = function(url) {
        return url.replace('&amp;', '&');
    }
};

DateTimePicker = new function () {
    this.Date = function (instance) {
        $("#" + instance).datetimepicker({
            locale: CultureInfo.getActive(),
            format: "DD.MM.YYYY"
        });
    }
    this.DateTime = function (instance) {
        $("#" + instance).datetimepicker({
            locale: CultureInfo.getActive(),
            format: "DD.MM.YYYY HH:mm:ss"
        });
    }
}