﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Enums;
using KIT.Helpers;
using KIT.Models;
using KIT.Repositories;
using Localization;
using Touring.ModelsVm;

namespace Touring.Processors
{
    public class TourProcessor:BaseProcessor
    {
        public TourProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        public InverseVm GetInverseVm(long id)
        {
            var lang = !string.IsNullOrEmpty(CookieHelper.Get("lang")) ? CookieHelper.Get("lang") : "ru";
            lang = lang != "en" ? "ru" : lang;
            var tour = CommonRepository.SingleOrDefault<Tour>(w => w.Id == id);
            if (lang == "ru")
            {
                return new InverseVm
                {
                    Tour = tour.NameTour,
                    TourId = tour.Id
                };
            }
            else
            {
                return new InverseVm
                {
                    Tour = tour.NameTour_en,
                    TourId = tour.Id
                };
            }
                
        }

        public void SaveInverse(InverseVm inverseVm)
        {
            var inverse = ConvertToInverse(inverseVm);
            CommonRepository.Add(inverse);
            CommonRepository.SaveChanges();
        }

        public InverseTour ConvertToInverse(InverseVm inverseVm)
        {
            return new InverseTour
            {
                BidStatus = ContactStatus.New,
                CityFly = inverseVm.CityFly,
                DateFly = inverseVm.DateFly,
                Email = inverseVm.Email,
                FullName = inverseVm.FullName,
                IpAdress = inverseVm.IpAdress,
                PeopleCount = inverseVm.PeopleCount,
                PhoneNumber = inverseVm.PhoneNumber,
                TourId = inverseVm.TourId
            };
        }

        public bool IsValidInverse(InverseVm viewModel, out Dictionary<string, string> errors)
        {
            var valid = true;
            errors = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(viewModel.Email))
            {
                errors.Add("Email", Resource.Укажите_почту);
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.PhoneNumber))
            {
                errors.Add("PhoneNumber", Resource.Укажите_номер_телефона);
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.FullName))
            {
                errors.Add("FullName", Resource.Введите_фио);
                valid = false;
            }
            if (viewModel.PeopleCount<=0)
            {
                errors.Add("PeopleCount", Resource.Количество_человек);
                valid = false;
            }
            if (viewModel.PeopleCount == null)
            {
                errors.Add("PeopleCount", Resource.Укажите_количество_человек);
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.CityFly))
            {
                errors.Add("CityFly", Resource.Укажите_город);
                valid = false;
            }

            return valid;
        }


        public TourPage GetData(long id)
        {
            var lang = !string.IsNullOrEmpty(CookieHelper.Get("lang")) ? CookieHelper.Get("lang") : "ru";
            lang = lang != "en" ? "ru" : lang;
            var contacts = CommonRepository.Entities<Contacts>().SingleOrDefault();
            var tours = CommonRepository.SingleOrDefault<Tour>(w=>w.Id==id);
            return ConvertToTourPage(lang, tours, contacts);
        }


        public TourPage ConvertToTourPage(string lang, Tour tour, Contacts contacts)
        {
            return new TourPage
            {
                MyTourVm = ConvertToMyTour(tour, lang),
                ContactVm = ConvertToContactVm(contacts)
            };
        }

        public ContactVm ConvertToContactVm(Contacts contacts)
        {
            return new ContactVm
            {
                Email = contacts.Email,
                Fb = contacts.Fb,
                Instagramm = contacts.Instagramm,
                Vk = contacts.Vk,
                Youtube = contacts.Youtube,
                PhoneNumber = contacts.PhoneNumber
            };
        }

        public MyTourVm ConvertToMyTour(Tour tour, string lang)
        {
            if (lang == "ru")
                return new MyTourVm
                {
                    Id = tour.Id,
                    NameTour = tour.NameTour,
                    Description = tour.Description
                };
            else
                return new MyTourVm
                {
                    Id = tour.Id,
                    NameTour = tour.NameTour_en,
                    Description = tour.Description_en
                };
        }

    }
}