﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Helpers;
using KIT.Models;
using KIT.Repositories;
using Touring.ModelsVm;

namespace Touring.Processors
{
    public class VideoProcessor:BaseProcessor
    {
        public VideoProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }


        public VideoContentVm GetData()
        {
            var lang = !string.IsNullOrEmpty(CookieHelper.Get("lang")) ? CookieHelper.Get("lang") : "ru";
            lang = lang != "en" ? "ru" : lang;
            var contacts = CommonRepository.Entities<Contacts>().SingleOrDefault();
            var video = CommonRepository.Entities<Video>().ToList();
            return ConvertVideoContentVm(lang, video, contacts);
        }

        public VideoContentVm ConvertVideoContentVm(string lang, List<Video> videos, Contacts contacts)
        {
            var VideoVmList = videos.Select(video => ConvertToVideoVm(video, lang)).ToList();

            return new VideoContentVm
            {
                ContactVm = ConvertToContactVm(contacts),
                VideoVm = VideoVmList
            };

        }

        public VideoVm ConvertToVideoVm(Video video, string lang)
        {
            if (lang == "ru")
                return new VideoVm
                {
                    Url = video.Url,
                    Name = video.Name
                };
            else
                return new VideoVm
                {
                    Url = video.Url,
                    Name = video.Name_en
                };
        }



        public ContactVm ConvertToContactVm(Contacts contacts)
        {
            return new ContactVm
            {
                Email = contacts.Email,
                Fb = contacts.Fb,
                Instagramm = contacts.Instagramm,
                Vk = contacts.Vk,
                Youtube = contacts.Youtube,
                PhoneNumber = contacts.PhoneNumber
            };
        }
    }
}