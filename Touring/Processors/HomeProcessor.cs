﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Enums;
using KIT.Helpers;
using KIT.Models;
using KIT.Repositories;
using Localization;
using Touring.ModelsVm;

namespace Touring.Processors
{
    public class HomeProcessor:BaseProcessor
    {
        public HomeProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        public InverseContactVm GetInverseContactVm(string email)
        {
            return new InverseContactVm
            {
                Email = email
            };

        }

        public void SaveInverseContact(InverseContactVm inverseContactVm)
        {
            var inverse = ConvertToInverse(inverseContactVm);
            CommonRepository.Add(inverse);
            CommonRepository.SaveChanges();
        }

        public InverseContact ConvertToInverse(InverseContactVm inverseVm)
        {
            return new InverseContact
            {
                BidStatus = ContactStatus.New,
                Email = inverseVm.Email,
                FullName = inverseVm.FullName,
                IpAdress = inverseVm.IpAdress,
                Number = inverseVm.Number
            };
        }


        public bool IsValidInverse(InverseContactVm viewModel, out Dictionary<string, string> errors)
        {
            var valid = true;
            errors = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(viewModel.Email))
            {
                errors.Add("Email", Resource.Укажите_почту);
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.Number))
            {
                errors.Add("Number", Resource.Укажите_номер_телефона);
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.FullName))
            {
                errors.Add("FullName", Resource.Введите_фио);
                valid = false;
            }

            return valid;
        }






        public HomePageVm GetData()
        {
            var lang = !string.IsNullOrEmpty(CookieHelper.Get("lang")) ? CookieHelper.Get("lang") : "ru";
            lang = lang != "en" ? "ru":lang;
            var welcome = CommonRepository.Entities<Welcome>().SingleOrDefault();
            var contacts = CommonRepository.Entities<Contacts>().SingleOrDefault();
            var aboutUs = CommonRepository.Entities<AboutUs>().SingleOrDefault();
            var tours = CommonRepository.Entities<Tour>().ToList();
            return ConverToHomePageVm(lang,aboutUs,welcome,tours,contacts);
        }




        public HomePageVm ConverToHomePageVm(string lang, AboutUs aboutUs, Welcome welcome,List<Tour> tours,Contacts contacts)
        {
           var tourList= tours.Select(tour => ConvertToTourVm(tour, lang)).ToList();

            return new HomePageVm
            {
                AboutUs = ConvertToAboutUsVm(aboutUs,lang),
                Tour = tourList,
                Welcome = ConvertToWelcomeVm(welcome,lang),
                Contact = ConvertToContactVm(contacts)
            };

        }

        public ContactVm ConvertToContactVm(Contacts contacts)
        {
            return new ContactVm
            {
                Email = contacts.Email,
                Fb = contacts.Fb,
                Instagramm = contacts.Instagramm,
                Vk = contacts.Vk,
                Youtube = contacts.Youtube,
                PhoneNumber = contacts.PhoneNumber
            };
        }

        public AboutUsVm ConvertToAboutUsVm(AboutUs aboutUs, string lang)
        {
            if (lang == "ru")
                return new AboutUsVm
                {
                    Text = aboutUs.Text
                };
            else
                return new AboutUsVm
                {
                    Text = aboutUs.Text_en
                };
        }

        public WelcomeVm ConvertToWelcomeVm(Welcome welcome, string lang)
        {
            if (lang=="ru")
                return new WelcomeVm
                {
                    Text = welcome.Text
                };
            else
                return new WelcomeVm
                {
                    Text = welcome.Text_en
                };
        }

        public TourVm ConvertToTourVm(Tour tour,string lang)
        {
            var uploadedFile = new UploadedFileVm
            {
                FileName = tour.UploadedFile.FileName,
                ContentType = tour.UploadedFile.ContentType,
                Bytes = tour.UploadedFile.File
            };

            if (lang== "ru")
                return new TourVm
                {
                   File = uploadedFile,
                   Id = tour.Id,
                   NameTour = tour.NameTour,
                   ShortDescription = tour.ShortDescription,
                };
            else
                return new TourVm
                {
                    File = uploadedFile,
                    Id = tour.Id,
                    NameTour = tour.NameTour_en,
                    ShortDescription = tour.ShortDescription_en,
                };

        }

        
    }
}