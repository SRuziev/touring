﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Helpers;
using KIT.Models;
using KIT.Repositories;
using Touring.ModelsVm;

namespace Touring.Processors
{
    public class IssuesProcessor:BaseProcessor
    {
        public IssuesProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        public IssuesVmPage GetData()
        {
            var lang = !string.IsNullOrEmpty(CookieHelper.Get("lang")) ? CookieHelper.Get("lang") : "ru";
            lang = lang != "en" ? "ru" : lang;
            var contacts = CommonRepository.Entities<Contacts>().SingleOrDefault();
            var issues = CommonRepository.Entities<IssuesClient>().ToList();
            var b = "";
            return ConverIssuesVmPage(lang, issues, contacts);
        }

        public IssuesVmPage ConverIssuesVmPage(string lang, List<IssuesClient> issuesClients, Contacts contacts)
        {
            var IssuesVmList = issuesClients.Select(issuesclietn => ConvertToIssuesVm(issuesclietn, lang)).ToList();

            return new IssuesVmPage
            {
                ContactVm = ConvertToContactVm(contacts),
                IssuesVm = IssuesVmList
            };

        }

        public IssuesVm ConvertToIssuesVm(IssuesClient issuesClient, string lang)
        {
            if (lang == "ru")
                return new IssuesVm
                {
                    Issues = issuesClient.Issues,
                    Answer = issuesClient.Answer
                };
            else
                return new IssuesVm
                {
                    Issues = issuesClient.Issues_en,
                    Answer = issuesClient.Answer_en
                };
        }



        public ContactVm ConvertToContactVm(Contacts contacts)
        {
            return new ContactVm
            {
                Email = contacts.Email,
                Fb = contacts.Fb,
                Instagramm = contacts.Instagramm,
                Vk = contacts.Vk,
                Youtube = contacts.Youtube,
                PhoneNumber = contacts.PhoneNumber
            };
        }


    }
}