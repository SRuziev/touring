﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Repositories;

namespace Touring.Processors
{
    public abstract class BaseProcessor
    {
        protected readonly ICommonRepository CommonRepository;

        public BaseProcessor(ICommonRepository commonRepository)
        {
            CommonRepository = commonRepository;
        }

    }
}