﻿using System.Web;
using System.Web.Optimization;

namespace Touring
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/layout").Include(
                "~/Content/assets/js/jquery.min.js",
                "~/Content/assets/js/bootstrap.min.js",
                "~/Content/assets/js/scripts.js",
                "~/Content/assets/js/jquery.isotope.js",
                "~/Content/assets/js/jquery.slicknav.js",
                "~/Content/assets/js/jquery.visible.js",
                "~/Content/assets/js/jquery.sticky.js",
                "~/Content/assets/js/slimbox2.js",
                "~/Scripts/jquery.cookie-1.4.1.min.js",
                "~/Scripts/jquery.cookie.min.js",
                "~/Scripts/moment.js",
                "~/Scripts/moment-with-locales.js",
                "~/Scripts/bootstrap-datetimepicker.js",
                "~/Scripts/CultureInfo.js",
                "~/Scripts/MaskHelper.js",
                "~/Scripts/CommonHelper.js",
                "~/Scripts/jquery.blockUI.js",
                "~/Scripts/bootbox.js",
                "~/Content/assets/js/modernizr.custom.js"
            ));



            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/Content/layout").Include(
                "~/Content/assets/css/bootstrap.css",
                "~/Content/assets/css/animate.css",
                "~/Content/assets/css/font-awesome.css",
                "~/Content/assets/css/nexus.css",
                "~/Content/assets/css/custom.css",
                "~/Content/bootstrap-datetimepicker.css",
                "~/Content/assets/css/responsive.css"
            ));
        }
    }
}
