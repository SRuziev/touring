﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KIT.Helpers;
using Touring.ModelsVm;
using Touring.Processors;

namespace Touring.Controllers
{
    public class HomeController : ClientController
    {
        private readonly HomeProcessor _homeProcessor;

        public HomeController()
        {
            _homeProcessor = new HomeProcessor(CommonRepository);
        }

        public ActionResult Index()
        {
            return View(_homeProcessor.GetData());
        }


        [HttpGet]
        public ActionResult AddInverse(string email)
        {
            return Json(new { html = this.PartialViewToString("_addinversecontact", _homeProcessor.GetInverseContactVm(email)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost,ValidateAntiForgeryToken]
        public ActionResult AddInverses(InverseContactVm inverseContactVm)
        {
            Dictionary<string, string> errors;
            if (!_homeProcessor.IsValidInverse(inverseContactVm, out errors))
                return Json(new { status = false, errors }, JsonRequestBehavior.AllowGet);
            inverseContactVm.IpAdress = Request.UserHostAddress;
            _homeProcessor.SaveInverseContact(inverseContactVm);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

    }
}