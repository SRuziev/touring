﻿using System;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Mvc;
using KIT.Enums;
using KIT.Enums.Extensions;
using KIT.Models;
using KIT.Repositories;
using Localization;
using Touring.Filters;

namespace Touring.Controllers
{
    [Culture]
    public class ClientController : Controller
    {
        protected readonly ICommonRepository CommonRepository;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var ip = HttpContext.Request.UserHostAddress;
            var ipadres = CommonRepository.SingleOrDefault<Security>(w => w.Ip == ip);
            if (ipadres != null)
            {
                filterContext.Result = new RedirectResult("/Ban");
                return;
            }
        }

        public ClientController()
        {
            CommonRepository = new CommonRepository();
        }

        protected override void Dispose(bool disposing)
        {
            CommonRepository.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet, AllowAnonymous]
        public ActionResult ChangeCulture(Language langEnum)
        {
            var lang = langEnum.ToLowerString();

            var cultures = Enum.GetValues(typeof(Language)).Cast<Language>().Select(w => w.ToLowerString()).ToList();
            if (!cultures.Contains(lang))
                lang = Language.Ru.ToLowerString();

            var cookie = Request.Cookies["lang"];
            if (cookie != null)
                cookie.Value = lang;
            else
            {
                cookie = new HttpCookie("lang")
                {
                    HttpOnly = false,
                    Value = lang,
                    Expires = DateTime.Now.AddYears(1)
                };
            }

            Response.Cookies.Add(cookie);
            if (Request.UrlReferrer == null)
                return RedirectToAction("Index", "Home");

            return Redirect(Request.UrlReferrer.PathAndQuery);
        }

        protected Language GetCurrentCulture
        {
            get
            {
                var cookie = Request.Cookies["lang"];
                return cookie?.Value.ToEnum<Language>() ?? Language.Ru;
            }
        }

        public ActionResult GetResourceByKey(string key)
        {
            var resourceValue = Resource.ResourceManager.GetString(key);
            return Json(new { value = resourceValue }, JsonRequestBehavior.AllowGet);
        }

    }
}