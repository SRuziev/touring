﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KIT.Helpers;
using Touring.ModelsVm;
using Touring.Processors;

namespace Touring.Controllers
{
    public class TourController : ClientController
    {

        private readonly TourProcessor _tourProcessor;

        public TourController()
        {
            _tourProcessor = new TourProcessor(CommonRepository);
        }

        // GET: Tour
        public ActionResult Index(long id)
        {
            return View(_tourProcessor.GetData(id));
        }


        [HttpGet]
        public ActionResult AddInverse(long id)
        {
            return Json(new { html = this.PartialViewToString("_addinverse", _tourProcessor.GetInverseVm(id)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost,ValidateAntiForgeryToken]
        public ActionResult AddInverses(InverseVm inverseVm)
        {
            Dictionary<string, string> errors;
            if (!_tourProcessor.IsValidInverse(inverseVm, out errors))
                return Json(new { status = false, errors }, JsonRequestBehavior.AllowGet);
            inverseVm.IpAdress=Request.UserHostAddress;
            _tourProcessor.SaveInverse(inverseVm);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

    }
}