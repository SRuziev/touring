﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Touring.Processors;

namespace Touring.Controllers
{
    public class IssuesController : ClientController
    {
        private readonly IssuesProcessor _issuesProcessor;

        public IssuesController()
        {
            _issuesProcessor = new IssuesProcessor(CommonRepository);
        }

        // GET: Issues
        public ActionResult Index()
        {
            return View(_issuesProcessor.GetData());
        }
    }
}