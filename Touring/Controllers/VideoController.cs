﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Touring.Processors;

namespace Touring.Controllers
{
    public class VideoController : ClientController
    {
        private readonly VideoProcessor _videoProcessor;

        public VideoController()
        {
            _videoProcessor = new VideoProcessor(CommonRepository);
        }


        // GET: Video
        public ActionResult Index()
        {
            return View(_videoProcessor.GetData());
        }
    }
}