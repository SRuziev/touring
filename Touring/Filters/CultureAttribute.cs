﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using KIT.Enums;
using KIT.Enums.Extensions;

namespace Touring.Filters
{
    public class CultureAttribute : FilterAttribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var cultureCookie = filterContext.HttpContext.Request.Cookies["lang"];
            var cultureName = cultureCookie != null ? cultureCookie.Value : Language.Ru.ToLowerString();

            var cultures = Enum.GetValues(typeof(Language)).Cast<Language>().Select(w => w.ToLowerString()).ToList();
            if (!cultures.Contains(cultureName))
                cultureName = Language.Ru.ToLowerString();

            var cultureInfo = new CultureInfo(cultureName);
            Thread.CurrentThread.CurrentCulture = cultureInfo;
            Thread.CurrentThread.CurrentUICulture = cultureInfo;

        }
    }
}