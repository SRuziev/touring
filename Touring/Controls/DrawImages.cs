﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Touring.ModelsVm;

namespace Touring.Controls
{
    public static class DrawImages
    {
        public static MvcHtmlString DrawImage(this HtmlHelper htmlHelper, UploadedFileVm uploadedFile, decimal width, decimal height)
        {
            var base64 = Convert.ToBase64String(uploadedFile.Bytes);
            var imgSrc = !string.IsNullOrEmpty(base64)
                ? $"data:{uploadedFile.ContentType};base64,{base64}" : "";

            var image = new TagBuilder("img");
            image.Attributes.Add("src", imgSrc);
            image.Attributes.Add("height", height.ToString(CultureInfo.InvariantCulture));
            image.Attributes.Add("width", width.ToString(CultureInfo.InvariantCulture));

            return new MvcHtmlString(image.ToString());
        }
    }
}