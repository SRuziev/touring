﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Touring.ModelsVm
{
    public class VideoVm
    {
        public string Name { get; set; }

        public string Url { get; set; }
    }
}