﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Touring.ModelsVm
{
    public class UploadedFileVm
    {
        public UploadedFileVm()
        {
            Bytes = new List<byte>().ToArray();
        }

        public HttpPostedFileBase Base { get; set; }

        public byte[] Bytes { get; set; }

        public string ContentType { get; set; }

        public string FileName { get; set; }
    }
}