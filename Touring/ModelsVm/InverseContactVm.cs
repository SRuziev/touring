﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using KIT.Enums;

namespace Touring.ModelsVm
{
    public class InverseContactVm
    {
        [Required, StringLength(60)]
        public string FullName { get; set; }

        [Required, StringLength(60)]
        public string Email { get; set; }

        [StringLength(35)]
        public string Number { get; set; }

        public string IpAdress { get; set; }

    }
}