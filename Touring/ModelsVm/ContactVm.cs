﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Touring.ModelsVm
{
    public class ContactVm
    {
        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Vk { get; set; }

        [Required]
        public string Fb { get; set; }

        [Required]
        public string Instagramm { get; set; }

        [Required]
        public string Youtube { get; set; }
    }
}