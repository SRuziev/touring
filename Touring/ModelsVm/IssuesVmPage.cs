﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Touring.ModelsVm
{
    public class IssuesVmPage
    {

        public IssuesVmPage()
        {
            var IssuesVm=new IssuesVm();
            var ContactVm = new ContactVm();
        }

        public List<IssuesVm> IssuesVm { get; set; }
        public ContactVm ContactVm { get; set; }
    }
}