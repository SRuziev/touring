﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Touring.ModelsVm
{
    public class HomePageVm
    {
        public HomePageVm()
        {
            var Contact = new ContactVm();
            var Welcome = new WelcomeVm();
            var AboutUs = new AboutUsVm();
            var Tour = new TourVm();
        }

        public ContactVm Contact { get; set; }

        public WelcomeVm Welcome { get; set; }

        public AboutUsVm AboutUs { get; set; }

        public List<TourVm> Tour { get; set; }
    }
}