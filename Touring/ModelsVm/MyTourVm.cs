﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Touring.ModelsVm
{
    public class MyTourVm
    {
        public long Id { get; set; }

        [Required, StringLength(100)]
        public string NameTour { get; set; }

        [Required]
        public string Description { get; set; }
    }
}