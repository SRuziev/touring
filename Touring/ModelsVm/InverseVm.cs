﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Touring.ModelsVm
{
    public class InverseVm
    {
        public InverseVm()
        {
            DateFly = DateTime.Now;
        }
        [Required, StringLength(70)]
        public string FullName { get; set; }

        [Required, StringLength(100)]
        public string Email { get; set; }

        [Required, StringLength(100)]
        public string PhoneNumber { get; set; }

        [Required]
        public long TourId { get; set; }

        public string Tour { get; set; }
        public string IpAdress { get; set; }

        [Required]
        public int PeopleCount { get; set; }

        [Required, StringLength(250)]
        public string CityFly { get; set; }

        [Required]
        public DateTime DateFly { get; set; }
    }
}