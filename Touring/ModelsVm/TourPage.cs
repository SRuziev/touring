﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Touring.ModelsVm
{
    public class TourPage
    {
        public TourPage()
        {
            var MyTourVm = new MyTourVm();
            var ContactVm = new ContactVm();
        }

        public MyTourVm MyTourVm { get; set; }
        public ContactVm ContactVm { get; set; }
    }
}