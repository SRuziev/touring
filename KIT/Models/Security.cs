﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.Models
{
    public class Security:PersistentObject
    {
        [Required, StringLength(100)]
        public string Ip { get; set; }

        public string Comment { get; set; }
    }
}
