﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.Models.BaseModels
{
    public class CCO
    {
        public string CssClass { get; set; }

        public string EventCallBack { get; set; }

        public string Placeholder { get; set; }

        public bool ContainerMaxWidth { get; set; }
    }
}
