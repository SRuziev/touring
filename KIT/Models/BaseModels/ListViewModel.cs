﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.Models.BaseModels
{
    public class ListViewModel<TViewItem, TFilters>
    {
        public IEnumerable<TViewItem> Items { get; set; }

        public TFilters Filters { get; set; }

        public PageInfo PageInfo { get; set; }
    }
}
