﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KIT.Enums;

namespace KIT.Models.BaseModels
{
    public class PageInfo
    {
        /// <summary>
        /// Поле по которому сортируем
        /// </summary>
        public string SortNameField { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortableType SortableType { get; set; }

        /// <summary>
        /// Номер текущей страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество записей на странице
        /// </summary>
        public PageSize PageSize { get; set; }

        /// <summary>
        /// Всего записей
        /// </summary>
        public int TotalItems { get; set; }

        /// <summary>
        /// Всего страниц
        /// </summary>
        public int TotalPages
        {
            get
            {
                try
                {
                    return (int)Math.Ceiling((decimal)TotalItems / (int)PageSize);
                }
                catch (Exception)
                {
                    return 1;
                }
            }
        }
    }
}
