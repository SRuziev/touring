﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.Models
{
    public class Application:PersistentObject
    {
        [Required, StringLength(60)]
        public string FullName { get; set; }
    }
}
