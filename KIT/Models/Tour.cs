﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.Models
{
    public class Tour:PersistentObject
    {
        [Required, StringLength(100)]
        public string NameTour { get; set; }

        [Required, StringLength(100)]
        public string NameTour_en { get; set; }

        [Required]
        public string ShortDescription { get; set; }

        [Required]
        public string ShortDescription_en { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string Description_en { get; set; }

        public bool IsDeleted { get; set; }

        [ForeignKey("UploadedFile")]
        public long? FileId { get; set; }
        public virtual UploadedFile UploadedFile { get; set; }
    }
}
