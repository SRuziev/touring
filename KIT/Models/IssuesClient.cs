﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.Models
{
    public class IssuesClient:PersistentObject
    {
        public string Issues { get; set; }

        public string Issues_en { get; set; }

        public string Answer { get; set; }

        public string Answer_en { get; set; }
    }
}
