﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KIT.Enums;

namespace KIT.Models
{
    public class InverseContact:PersistentObject
    {
        [Required, StringLength(60)]
        public string FullName { get; set; }

        [Required, StringLength(60)]
        public string Email { get; set; }

        [StringLength(35)]
        public string Number { get; set; }

        public string IpAdress { get; set; }

        public ContactStatus BidStatus { get; set; }
    }
}
