﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.Models
{
    public class AdminAccount:PersistentObject
    {
        [Required, StringLength(100)]
        public string Login { get; set; }

        [Required, StringLength(100)]
        public string Password { get; set; }

        public bool IsActiv { get; set; }

        [Required, StringLength(160)]
        public string FullName { get; set; }
    }
}
