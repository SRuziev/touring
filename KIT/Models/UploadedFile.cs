﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.Models
{
    public class UploadedFile : PersistentObject
    {
        [StringLength(255)]
        public string FileName { set; get; }

        public byte[] File { set; get; }

        [StringLength(50)]
        public string ContentType { set; get; }
    }
}
