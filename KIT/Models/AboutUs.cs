﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.Models
{
    public class AboutUs:PersistentObject
    {
        public string Text { get; set; }

        public string Text_en { get; set; }
    }
}
