﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace KIT.Helpers
{
    public static class DateTimeExntensions
    {
        public static string ToLocalSystemFormat(this DateTime value)
        {
            return value.ToString("dd.MM.yyyy HH:mm:ss");
        }
        public static DateTime StartDay(this DateTime value)
        {
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0, DateTimeKind.Local);
        }

        public static DateTime EndDay(this DateTime value)
        {
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59, 999, DateTimeKind.Local);
        }

        public static DateTime PathToDateTime(this string w)
        {
            var lengthSeparator = w.Split('\\').Length;
            var date = w.Split('\\')[lengthSeparator - 2];
            var time = Path.GetFileName(w).Split('.').First();

            DateTime parseTime;
            if (DateTime.TryParseExact(date + " " + time, "yyyy-MM-dd HH", CultureInfo.InvariantCulture, DateTimeStyles.None, out parseTime))
                return parseTime;

            if (DateTime.TryParseExact(date + " " + time, "yyyy-MM-dd HH-mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out parseTime))
                return parseTime;

            if (DateTime.TryParseExact(date + " " + time, "yyyy-MM-dd HH_mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out parseTime))
                return parseTime;

            if (DateTime.TryParseExact(date + " " + time, "yyyy-MM-dd HH-mm-ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out parseTime))
                return parseTime;

            if (DateTime.TryParseExact(date + " " + time, "yyyy-MM-dd HH_mm_ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out parseTime))
                return parseTime;

            return parseTime;
        }
    }
}
