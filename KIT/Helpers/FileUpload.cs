﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using KIT.ModelsVm;

namespace KIT.Helpers
{
    public static partial class CustomControl
    {
        public static MvcHtmlString FileUpload(this HtmlHelper htmlHelper, string name, UploadedFileVm model, string let = null)
        {
            var fileName = !string.IsNullOrEmpty(model.FileName) ? model.FileName : "Выберите файл";

            var button = "<button type=\"button\"><i class=\"fa fa-folder-open\" aria-hidden=\"true\"></i></button>";
            var label = $"<div>{fileName}</div>";
            var file = $"<input type=\"file\" name=\"{name}.Base\" id=\"{name}_Base\" let=\"{let}\" />";

            var container = $"<div class=\"file_upload\">{button}{label}{file}</div>";

            return new MvcHtmlString(container);
        }

        public static MvcHtmlString FileUploadFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, UploadedFileVm>> expression, string let = null)
        {
            var fieldName = ExpressionHelper.GetExpressionText(expression);
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var model = (UploadedFileVm)metadata.Model;

            var fileName = model != null && !string.IsNullOrEmpty(model.FileName) ? model.FileName : "Выберите файл";

            var button = "<button type=\"button\"><i class=\"fa fa-folder-open\" aria-hidden=\"true\"></i></button>";
            var label = $"<div>{fileName}</div>";
            var file = $"<input type=\"file\" name=\"{fieldName}.Base\" id=\"{fieldName}_Base\" let=\"{let}\" />";

            var container = $"<div class=\"file_upload\">{button}{label}{file}</div>";

            return new MvcHtmlString(container);
        }
    }
}
