﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using KIT.Models;
using KIT.Repositories;

namespace KIT.Helpers
{
    public static class AccessContext
    {
        public static string GetIdentityLogin => HttpContext.Current.User.Identity.Name;

        public static bool IsAuthenticated => HttpContext.Current.User.Identity.IsAuthenticated;

        /// <summary>
        /// Получить ID пользователя
        /// </summary>
        public static long? AccountId => GetAccount?.Id;

        /// <summary>
        /// Получить имя пользователя
        /// </summary>
        public static string GetUserName => GetAccount?.FullName;

        /// <summary>
        /// Получить аккаунт
        /// </summary>
        public static AdminAccount GetAccount
        {
            get
            {
                if (!IsAuthenticated)
                    return null;

                if (String.IsNullOrEmpty(GetIdentityLogin))
                    return null;

                using (var dbLayer = new CommonRepository())
                {
                    var baseAccount = dbLayer.SingleOrDefault<AdminAccount>(w => w.Login == GetIdentityLogin);
                    return baseAccount;
                }
            }
        }
    }
}
