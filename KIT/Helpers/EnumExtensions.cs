﻿using System;
using System.ComponentModel;

namespace KIT.Helpers
{
    public static class EnumExtensions
    {
        public static string Description(this System.Enum value)
        {
            try
            {
                var attrs = value.GetType().GetMember(System.Enum.GetName(value.GetType(), value))[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                return attrs.Length <= 0 ? null : ((DescriptionAttribute)attrs[0]).Description;
            }
            catch (Exception)
            {
                return "ERROR";
            }
        }

        public static bool HasAttribute<TEnum>(TEnum value, Type attributeType)
        {
            var fi = value.GetType().GetField(value.ToString());
            var attributes = fi.GetCustomAttributes(attributeType, false);
            return (attributes.Length > 0);
        }

        public static string GetEnumDescription<TEnum>(TEnum value)
        {
            var fi = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
        }
    }
}
