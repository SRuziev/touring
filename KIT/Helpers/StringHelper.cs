﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace KIT.Helpers
{
    public static class StringHelper
    {
        public static string ToLowerString(this XmlNode node)
        {
            var answer = node.InnerText;
            return answer.ToLower();
        }

        public static string ToId(this string @string)
        {
            @string = @string.Contains('.') ? @string.Replace('.', '_') : @string;
            @string = @string.Contains('[') ? @string.Replace('[', '_') : @string;
            @string = @string.Contains(']') ? @string.Replace(']', '_') : @string;
            return @string;
        }

        public static string IgnoreWhitespace(this string inputString)
        {
            return string.IsNullOrEmpty(inputString) ? null : Regex.Replace(inputString, @"\s+", "");
        }
    }
}
