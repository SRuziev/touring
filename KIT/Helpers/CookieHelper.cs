﻿using System;
using System.Web;

namespace KIT.Helpers
{
    public static class CookieHelper
    {
        public static string Get(string key)
        {
            var cookie = HttpContext.Current.Request.Cookies[key];
            return cookie != null ? cookie.Value : string.Empty;
        }

        public static void Set(string key, object value)
        {
            var cookie = HttpContext.Current.Request.Cookies[key];

            if (cookie != null)
                cookie.Value = value.ToString();
            else
            {
                cookie = new HttpCookie(key)
                {
                    HttpOnly = false,
                    Value = value.ToString(),
                    Expires = DateTime.Now.AddYears(1)
                };
            }

            HttpContext.Current.Response.Cookies.Add(cookie);
        }
    }
}