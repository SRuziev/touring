﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Xml.Linq;
using KIT.Models.BaseModels;

namespace KIT.Helpers.Controls
{
    public static class GroupControls
    {
        //<iframe width = "560" height="315" src="https://www.youtube.com/embed/pCavsKZ7_Cs" frameborder="0" allowfullscreen></iframe>

        public static MvcHtmlString YoutubePlay(this HtmlHelper htmlHelper, string url, int width, int height)
        {
            return new MvcHtmlString($"<div class=\"video_play\"><iframe width = \"{width}\" height= \"{height}\" src=\"{url}\" frameborder=\"0\" allowfullscreen></iframe></div>");
        }

         
        public static MvcHtmlString DrawXml(this HtmlHelper htmlHelper, string xml)
        {
            return new MvcHtmlString(DrawXml(xml).Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "'"));
        }

        private static string DrawXml(string xml)
        {
            string toString;
            try
            {
                var xDocument = XDocument.Parse(xml);
                toString = xDocument.ToString();
            }
            catch (Exception)
            {
                toString = string.IsNullOrEmpty(xml) ? string.Empty : xml;
            }

            return toString;
        }

        public static MvcHtmlString GroupDateTimeRangeFor<TModel>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, DateTime>> expressionForStart, Expression<Func<TModel, DateTime>> expressionForEnd,
            string label, bool onlyDate = false, CCO config = null)
        {
            var cco = config ?? new CCO();

            var labelFor = htmlHelper.LabelFor(expressionForStart, label, new { @class = "control-label" });
            var textBox1For = htmlHelper.DateTimePickerFor(expressionForStart, cco, onlyDate);
            var textBox2For = htmlHelper.DateTimePickerFor(expressionForEnd, cco, onlyDate);

            var stringBuilder = new StringBuilder();
            stringBuilder.Append(labelFor);
            stringBuilder.Append("<div class=\"input-group input-large\">");
            stringBuilder.Append(textBox1For);
            stringBuilder.Append($"<span class=\"input-group-addon\">До</span>");
            stringBuilder.Append(textBox2For);
            stringBuilder.Append("</div>");

            return new MvcHtmlString($"<div class=\"form-group\">{stringBuilder}</div>");
        }

        public static MvcHtmlString GroupDateTimeRangeFor<TModel>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, DateTime?>> expressionForStart, Expression<Func<TModel, DateTime?>> expressionForEnd,
            string label, bool onlyDate = false, CCO config = null)
        {
            var cco = config ?? new CCO();

            var labelFor = htmlHelper.LabelFor(expressionForStart, label, new { @class = "control-label" });
            var textBox1For = htmlHelper.DateTimePickerFor(expressionForStart, cco, onlyDate);
            var textBox2For = htmlHelper.DateTimePickerFor(expressionForEnd, cco, onlyDate);

            var stringBuilder = new StringBuilder();
            stringBuilder.Append(labelFor);
            stringBuilder.Append("<div class=\"input-group input-large\">");
            stringBuilder.Append(textBox1For);
            stringBuilder.Append($"<span class=\"input-group-addon\">До</span>");
            stringBuilder.Append(textBox2For);
            stringBuilder.Append("</div>");

            return new MvcHtmlString($"<div class=\"form-group\">{stringBuilder}</div>");
        }

        public static MvcHtmlString DateTimePickerFor<TModel>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, DateTime>> expression, CCO controlOptions = null, bool? onlyDate = null)
        {
            var fieldName = ExpressionHelper.GetExpressionText(expression);
            var fieldId = fieldName.ToId();
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var model = (DateTime)metadata.Model;

            var htmlAttrs = new Dictionary<string, object>
            {
                {"class", $"form-control {controlOptions?.CssClass}"},
                {"placeholder", controlOptions?.Placeholder},
            };

            var textBox = htmlHelper.TextBox(fieldName, model.ToString("dd.MM.yyyy HH:mm:ss"), htmlAttrs);
            var func = onlyDate.HasValue && onlyDate.Value
                ? $"DateTimePicker.Date('{fieldId}');"
                : $"DateTimePicker.DateTime('{fieldId}');";

            var script = $"<script>$(document).ready(function() {{ {func} }});</script>";
            return new MvcHtmlString(textBox + script);
        }

        public static MvcHtmlString DateTimePickerFor<TModel>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, DateTime?>> expression, CCO controlOptions = null, bool? onlyDate = null)
        {
            var fieldName = ExpressionHelper.GetExpressionText(expression);
            var fieldId = fieldName.ToId();
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var model = (DateTime?)metadata.Model;

            var htmlAttrs = new Dictionary<string, object>
            {
                {"class", $"form-control {controlOptions?.CssClass}"},
                {"placeholder", controlOptions?.Placeholder},
            };

            var textBox = htmlHelper.TextBox(fieldName, model?.ToString("dd.MM.yyyy HH:mm:ss"), htmlAttrs);
            var func = onlyDate.HasValue && onlyDate.Value
                ? $"DateTimePicker.Date('{fieldId}');"
                : $"DateTimePicker.DateTime('{fieldId}');";

            var script = $"<script>$(document).ready(function() {{ {func} }});</script>";
            return new MvcHtmlString(textBox + script);
        }
    }
}
