﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using KIT.ModelsVm;


namespace KIT.Helpers.Controls
{
    public static partial class CustomControl
    {
        public static MvcHtmlString DrawImage(this HtmlHelper htmlHelper, UploadedFileVm uploadedFile, decimal width, decimal height)
        {
            var base64 = Convert.ToBase64String(uploadedFile.Bytes);
            var imgSrc = !string.IsNullOrEmpty(base64)
                ? $"data:{uploadedFile.ContentType};base64,{base64}":"";

            var image = new TagBuilder("img");
            image.Attributes.Add("src", imgSrc);
            image.Attributes.Add("height", height.ToString(CultureInfo.InvariantCulture));
            image.Attributes.Add("width", width.ToString(CultureInfo.InvariantCulture));

            return new MvcHtmlString(image.ToString());
        }

        
        public static MvcHtmlString DrawImageFor<TModel>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, UploadedFileVm>> expression, decimal width, decimal height)
        {
            var fieldName = ExpressionHelper.GetExpressionText(expression);
            var fieldId = fieldName.ToId();
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var model = (UploadedFileVm)metadata.Model;

            var base64 = Convert.ToBase64String(model.Bytes);
            var imgSrc = !string.IsNullOrEmpty(base64)
                ? $"data:{model.ContentType};base64,{base64}":"";

            var image = new TagBuilder("img");
            image.Attributes.Add("src", imgSrc);
            image.Attributes.Add("height", height.ToString(CultureInfo.InvariantCulture));
            image.Attributes.Add("width", width.ToString(CultureInfo.InvariantCulture));

            return new MvcHtmlString(image.ToString());
        }
    }
}
