﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using KIT.Enums;
using KIT.Models.BaseModels;

namespace KIT.Helpers.Controls
{
    public static partial class CustomControl
    {
        public static MvcHtmlString PageSize(this HtmlHelper htmlHelper, PageInfo pageInfo)
        {
            var textCountOnPage = $"<span class='showing-PageSize-text'>Записей на странице</span>";
            const string fieldName = "PageInfo.PageSize";
            const string id = "PageInfo_PageSize";

            var attributes = new Dictionary<string, object> { { "class", " hide" } };
            var items = new List<SelectListItem>();
            var itemsShell = "";
            var pageSizes = Enum.GetValues(typeof(PageSize)).Cast<PageSize>().ToList();

            foreach (var value in pageSizes)
            {
                var selectListItem = new SelectListItem
                {
                    Text = EnumExtensions.GetEnumDescription(value),
                    Value = value.ToString(),
                    Selected = value == pageInfo.PageSize
                };
                items.Add(selectListItem);
                itemsShell += $"<li><a data-for-select='{id}' data-for-select-value='{value}' " +
                              $"onclick='SelectValueCustom.sendAttr(this); Paginator.SUBMIT();'>{EnumExtensions.GetEnumDescription(value)}</a></li>";
            }

            var selectedItem = items.FirstOrDefault(x => x.Selected) ?? new SelectListItem { Text = items.First().Text, Selected = true };

            var input = string.Format(
                "<input data-toggle='dropdown' class='dropdown-toggle form-control readonly caret-down' type='text' data-for-select-button='{0}' value='{1}' readonly='readonly'/>" +
                "<ul role='menu' class='dropdown-menu' data-for-select-list='{0}'>{2}</ul>",
                id, selectedItem.Text, itemsShell);

            var script = $"<script>$(document).ready(function () {{ SelectValueCustom.Subscribe(\"{id}\"); }});</script>";
            var html = $"<div class='btn-group showing-PageSize'>{htmlHelper.DropDownList(fieldName, items, null, attributes)}{input}{script}</div>";
            var hiddensSort = $"{htmlHelper.Hidden("PageInfo.SortNameField")}{htmlHelper.Hidden("PageInfo.SortableType")}";

            return new MvcHtmlString(html + textCountOnPage + hiddensSort);
        }

        public static MvcHtmlString Paginator(this HtmlHelper html, PageInfo pageInfo)
        {
            var pageNumber = $"<input type='hidden' name='PageInfo.PageNumber' id='PageInfo_PageNumber' value='{pageInfo.PageNumber}' />";

            var paginators = pageInfo.PageNumber > 1
                ? $"<li class='prev'><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber - 1}');\">← Предыдущая</a></li>"
                : $"<li class='prev disabled'><a href=\"javascript:;\">← Предыдущая</a></li>";

            if (pageInfo.PageNumber >= 5 && pageInfo.PageNumber == pageInfo.TotalPages)
            {
                paginators +=
                    $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber - 4}');\">{pageInfo.PageNumber - 4}</a></li>" +
                    $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber - 3}');\">{pageInfo.PageNumber - 3}</a></li>" +
                    $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber - 2}');\">{pageInfo.PageNumber - 2}</a></li>" +
                    $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber - 1}');\">{pageInfo.PageNumber - 1}</a></li>";
            }
            else
            if (pageInfo.PageNumber >= 4 && (pageInfo.PageNumber + 1 == pageInfo.TotalPages || pageInfo.PageNumber == pageInfo.TotalPages))
            {
                paginators +=
                    $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber - 3}');\">{pageInfo.PageNumber - 3}</a></li>" +
                    $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber - 2}');\">{pageInfo.PageNumber - 2}</a></li>" +
                    $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber - 1}');\">{pageInfo.PageNumber - 1}</a></li>";
            }
            else
            if (pageInfo.PageNumber > 3)
            {
                paginators +=
                    $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber - 2}');\">{pageInfo.PageNumber - 2}</a></li>" +
                    $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber - 1}');\">{pageInfo.PageNumber - 1}</a></li>";
            }
            else
            if (pageInfo.PageNumber == 2)
            {
                paginators += $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber - 1}');\">{pageInfo.PageNumber - 1}</a></li>";
            }
            else
            if (pageInfo.PageNumber == 3)
            {
                paginators +=
                    $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber - 2}');\">{pageInfo.PageNumber - 2}</a></li>" +
                    $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber - 1}');\">{pageInfo.PageNumber - 1}</a></li>";
            }

            if (pageInfo.PageNumber == 0)
                pageInfo.PageNumber = 1;

            paginators += $"<li class=\"active\"><a href=\"javascript:;\">{pageInfo.PageNumber}</a></li>";

            if (pageInfo.PageNumber + 1 <= pageInfo.TotalPages)
                paginators += $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber + 1}');\">{pageInfo.PageNumber + 1}</a></li>";

            if (pageInfo.PageNumber + 2 <= pageInfo.TotalPages)
                paginators += $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber + 2}');\">{pageInfo.PageNumber + 2}</a></li>";

            if (pageInfo.PageNumber == 1 && pageInfo.PageNumber + 3 <= pageInfo.TotalPages)
                paginators += $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber + 3}');\">{pageInfo.PageNumber + 3}</a></li>";

            if (pageInfo.PageNumber == 1 && pageInfo.PageNumber + 4 <= pageInfo.TotalPages)
                paginators += $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber + 4}');\">{pageInfo.PageNumber + 4}</a></li>";

            if (pageInfo.PageNumber == 2 && pageInfo.PageNumber + 3 <= pageInfo.TotalPages)
                paginators += $"<li><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber + 3}');\">{pageInfo.PageNumber + 3}</a></li>";

            paginators += pageInfo.PageNumber < pageInfo.TotalPages
                ? $"<li class='prev'><a href=\"javascript:Paginator.SetPageNumber('{pageInfo.PageNumber + 1}');\">Следующая →</a></li>"
                : $"<li class='prev disabled'><a href=\"javascript:;\">Следующая →</a></li>";

            var pagerInfo = $"<div class=\"span6\"><div class=\"dataTables_info\">{string.Format("Количество записей на странице", pageInfo.TotalItems)}</div></div>";

            return MvcHtmlString.Create($"{pageNumber}<div class=\"row-fluid\">{pagerInfo}<div class=\"span6\"><div class='dataTables_paginate paging_bootstrap pagination'><ul>{paginators}</ul></div></div></div><div class=\"clearfix\"></div>");
        }

        public static MvcHtmlString FieldSortByTable(this HtmlHelper htmlHelper, PageInfo pageInfo, string fieldName, string description)
        {
            const string onclick = "Paginator.SetNameForSortField(this); Paginator.SUBMIT();";

            var sortType = SortableType.None;
            if (pageInfo.SortNameField == fieldName)
                sortType = pageInfo.SortableType;

            var icon = sortType == SortableType.Asc ? "-asc" : sortType == SortableType.Desc ? "-desc" : string.Empty;
            var link = $"<a href=\"javascript:;\" data-sort=\"{sortType}\" data-field=\"{fieldName}\" onclick=\"{onclick}\" class=\"sortable-th\">{description} <i class=\"fa fa-sort{icon}\"></i></a>";
            return new MvcHtmlString(link);
        }
    }
}
