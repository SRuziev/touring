﻿using System;
using System.Data;
using System.Data.Entity;

namespace KIT.Repositories
{
    public class SmartTransaction : IDisposable
    {
        private readonly DbContext _context;

        public SmartTransaction(DbContext context)
        {
            _context = context;
        }

        private int CountActiveTransaction { set; get; }

        private DbContextTransaction _dbTransaction;
        public SmartTransaction Get(IsolationLevel level = IsolationLevel.ReadCommitted)
        {
            if (CountActiveTransaction == 0)
                _dbTransaction = _context.Database.BeginTransaction(level);
            CountActiveTransaction += 1;
            return this;
        }

        public void Commit()
        {
            if (_dbTransaction == null)
                throw new Exception("Коммит не активной транзакции");
            CountActiveTransaction -= 1;

            if (CountActiveTransaction == 0)
                _dbTransaction.Commit();
        }


        public void Rollback()
        {
            if (_dbTransaction == null)
                throw new Exception("Откатывание не активной транзакции");
            CountActiveTransaction -= 1;

            if (CountActiveTransaction <= 0)
                _dbTransaction.Rollback();
        }

        public void Dispose()
        {
            if (CountActiveTransaction <= 0)
            {
                _dbTransaction.Dispose();
            }
        }

    }
}