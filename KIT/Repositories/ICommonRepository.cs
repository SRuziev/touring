﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KIT.Repositories
{
    public interface ICommonRepository : IDisposable
    {        
        IQueryable<T> Entities<T>() where T : class;
        T SingleOrDefault<T>(Expression<Func<T, bool>> expression) where T : class;
        void Add<T>(T entity) where T : class;
        void AddRange<T>(IEnumerable<T> list) where T : class;
        void Update<T>(T entity) where T : class;
        void Remove<T>(T entity) where T : class;
        void RemoveRange<T>(IEnumerable<T> list) where T : class;
        SmartTransaction SmartTransaction { get; }
        void SaveChanges();
        T SingleNativeQuery<T>(string query);        
    }
}