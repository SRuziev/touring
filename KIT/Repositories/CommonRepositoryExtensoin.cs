﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace KIT.Repositories
{
    public static class CommonRepositoryExtensoin
    {
        public static IQueryable<T> IncludeDependense<T, TProperty>(this IQueryable<T> source, Expression<Func<T, TProperty>> predicate) where T : class
        {
            return source.Include(predicate);
        }
    }
}