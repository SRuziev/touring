using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace KIT.Repositories
{
    public class CommonRepository : ICommonRepository
    {

        private readonly TouringDbContext _dbContext;        
        public CommonRepository()
        {
            _dbContext = new TouringDbContext();
            SmartTransaction = new SmartTransaction(_dbContext);
        }

        public SmartTransaction SmartTransaction { private set; get; }        

        public IQueryable<T> Entities<T>() where T : class
        {
            return _dbContext.Set<T>();
        }

        public T SingleOrDefault<T>(Expression<Func<T, bool>> expression) where T : class
        {
            return _dbContext.Set<T>().SingleOrDefault(expression);
        }
        
        public void Add<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Add(entity);
        }

        public void AddRange<T>(IEnumerable<T> list) where T : class
        {
            _dbContext.Set<T>().AddRange(list);
        }

        public void Update<T>(T entity) where T : class
        {            
            _dbContext.Set<T>().Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Remove<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Remove(entity);
        }

        public void RemoveRange<T>(IEnumerable<T> list) where T : class
        {
            _dbContext.Set<T>().RemoveRange(list);
        }

        public void SaveChanges()
        {
            try
            {
                _dbContext.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                var outputLines = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    outputLines.AppendLine(
                        $"{DateTime.Now}: Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:");
                    foreach (var ve in eve.ValidationErrors)
                        outputLines.AppendLine($"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"");
                }

                throw new Exception(outputLines.ToString());
            }
        }

        public static ICommonRepository Instance => new CommonRepository();


        public T SingleNativeQuery<T>(string query)
        {
            return _dbContext.Database.SqlQuery<T>(query).FirstOrDefault();
        }    

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
    }
}