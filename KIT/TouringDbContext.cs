﻿using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using KIT.Models;

namespace KIT
{
   public class TouringDbContext : DbContext
    {
        public TouringDbContext(): base(ConfigurationManager.AppSettings["EntitiesDataBaseCatalog"]) 
        {
            // при возникновении ошибки
            // The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer' 
            // registered in the application config file for the ADO.NET provider with invariant name 'System.Data.SqlClient' could not be loaded.
            var _ = typeof(System.Data.Entity.SqlServer.SqlProviderServices);

            // установка инициализации бд - стратегии миграции IDatabaseInitializer
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<TouringDbContext, Configuration>());
        }

        public DbSet<AdminAccount> AdminAccount { get; set; }
        public DbSet<Security> Security { get; set; }
        public DbSet<InverseContact> InverseContact { get; set; }
        public DbSet<Tour> Tour { get; set; }
        public DbSet<UploadedFile> UploadedFile { get; set; }
        public DbSet<IssuesClient> IssuesClient { get; set; }
        public DbSet<Contacts> Contacts { get; set; }
        public DbSet<InverseTour> InverseTour { get; set; }
        public DbSet<AboutUs> AboutUs { get; set; }
        public DbSet<Welcome> Welcome { get; set; }
        public DbSet<Video> Video { get; set; }




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}