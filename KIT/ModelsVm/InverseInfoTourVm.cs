﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using KIT.Enums;
using KIT.Models;
using KIT.Repositories;

namespace KIT.ModelsVm
{
    public class InverseInfoTourVm
    {
        public long id { get; set; }

        public DateTime CreateDate { get; set; }

        [Required, StringLength(70)]
        public string FullName { get; set; }

        [Required, StringLength(100)]
        public string Email { get; set; }

        [Required, StringLength(100)]
        public string PhoneNumber { get; set; }

        [Required]
        public string NameTour { get; set; }

        public long TourId { get; set; }

        [Required]
        public int PeopleCount { get; set; }

        [Required, StringLength(250)]
        public string CityFly { get; set; }

        [Required]
        public DateTime DateFly { get; set; }

        public string IpAdress { get; set; }

        public ContactStatus BidStatus { get; set; }

        public List<SelectListItem> Tours
        {
            get
            {
                using (var commonRepository = CommonRepository.Instance)
                {
                    var tours = commonRepository.Entities<Tour>().Where(w => !w.IsDeleted).Select(w => new { w.Id, w.NameTour }).ToList();
                    return tours.Select(w => new SelectListItem { Value = w.Id.ToString(), Text = w.NameTour }).ToList();
                }
            }
        }
    }
}
