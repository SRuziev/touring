﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.ModelsVm
{
    public class AdminAccountVm
    {
        public long Id { get; set; }
        
        public string Login { get; set; }
        
        public string Password { get; set; }
        public string OldPassword { get; set; }
        
        public string NewPassword { get; set; }
        
        public string RepeatPassword { get; set; }

        public string FullName { get; set; }
    }
}
