﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KIT.Models;

namespace KIT.ModelsVm
{
    public class TourVm
    {
        public TourVm()
        {
            File = new UploadedFileVm();
        }
        public long? Id { get; set; } 

        [Required, StringLength(100)]
        public string NameTour { get; set; }

        [Required, StringLength(100)]
        public string NameTour_en { get; set; }

        [Required]
        public string ShortDescription { get; set; }

        [Required]
        public string ShortDescription_en { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string Description_en { get; set; }

        public UploadedFileVm File { get; set; }
    }
}
