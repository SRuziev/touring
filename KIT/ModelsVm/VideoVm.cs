﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.ModelsVm
{
    public class VideoVm
    {
        public string Name { get; set; }

        public string Name_en { get; set; }

        public string Url { get; set; }
    }
}
