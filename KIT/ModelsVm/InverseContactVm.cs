﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using KIT.Enums;
using KIT.Helpers;

namespace KIT.ModelsVm
{
    public class InverseContactVm
    {
        public long Id { get; set; }

        [Required, StringLength(60)]
        public string FullName { get; set; }

        [Required, StringLength(60)]
        public string Email { get; set; }

        [StringLength(35)]
        public string Number { get; set; }

        public string IpAdress { get; set; }

        public ContactStatus BidStatus { get; set; }

        public DateTime CreateDate { get; set; }
    }

    public class InverseFilter
    {
        public InverseFilter()
        {
            DateStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            DateEnd = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
        }

        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public ContactStatus? BidStatus { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }

        public List<SelectListItem> Statuses
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                list.Add(new SelectListItem { Value = ContactStatus.Success.ToString(), Text = ContactStatus.Success.Description() });
                list.Add(new SelectListItem { Value = ContactStatus.New.ToString(), Text = ContactStatus.New.Description() });
                list.Add(new SelectListItem { Value = ContactStatus.Rejected.ToString(), Text = ContactStatus.Rejected.Description() });
                return list;
            }
        }


    }
}
