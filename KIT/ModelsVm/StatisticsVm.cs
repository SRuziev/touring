﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.ModelsVm
{
    public class StatisticsVm
    {
        public int NewInverseContact { get; set; }
        public int SuccsuesInverseContact { get; set; }
        public int RejectedInverseContact { get; set; }

        public int NewInverseTour { get; set; }
        public int SuccsuesInverseTour { get; set; }
        public int RejectedInverseTour { get; set; }
    }
}
