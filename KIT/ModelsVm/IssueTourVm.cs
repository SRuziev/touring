﻿namespace KIT.ModelsVm
{
    public class IssueTourVm
    {
        public long? Id { get; set; }

        public string Issues { get; set; }

        public string Issues_en { get; set; }

        public string Answer { get; set; }

        public string Answer_en { get; set; }
    }
}