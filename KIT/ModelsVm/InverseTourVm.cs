﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using KIT.Enums;
using KIT.Helpers;
using KIT.Models;
using KIT.Repositories;

namespace KIT.ModelsVm
{
    public class InverseTourVm
    {
        public long id { get; set; }

        public DateTime CreateDate { get; set; }

        [Required, StringLength(70)]
        public string FullName { get; set; }

        [Required]
        public string TourName { get; set; }

        public ContactStatus BidStatus { get; set; }
        
    }
    public class InverseTourFilter
    {
        public InverseTourFilter()
        {
            DateStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            DateEnd = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
        }

        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public ContactStatus? BidStatus { get; set; }

        public string FullName { get; set; }

        public long? Tour { get; set; }


        public List<SelectListItem> Statuses
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                list.Add(new SelectListItem { Value = ContactStatus.Success.ToString(), Text = ContactStatus.Success.Description() });
                list.Add(new SelectListItem { Value = ContactStatus.New.ToString(), Text = ContactStatus.New.Description() });
                list.Add(new SelectListItem { Value = ContactStatus.Rejected.ToString(), Text = ContactStatus.Rejected.Description() });
                return list;
            }
        }

        public List<SelectListItem> Tours
        {
            get
            {
                using (var commonRepository = CommonRepository.Instance)
                {
                    var tours = commonRepository.Entities<Tour>().Where(w=>!w.IsDeleted).Select(w => new { w.Id, w.NameTour }).ToList();
                    return tours.Select(w => new SelectListItem { Value = w.Id.ToString(), Text = w.NameTour }).ToList();
                }
            }
        }


    }
}
