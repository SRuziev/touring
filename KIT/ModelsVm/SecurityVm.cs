﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.ModelsVm
{
    public class SecurityVm
    {
        [Required, StringLength(100)]
        public string Ip { get; set; }

        public string Comment { get; set; }

        public long id { get; set; }
    }
}
