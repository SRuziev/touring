﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using KIT.Enums;
using KIT.Helpers;

namespace KIT.ModelsVm
{
    public class InverseStatusVm
    {
        public long id { get; set; }

        public ContactStatus InverseStatus { get; set; }

        public List<SelectListItem> Statuses
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                list.Add(new SelectListItem { Value = ContactStatus.Success.ToString(), Text = ContactStatus.Success.Description() });
                list.Add(new SelectListItem { Value = ContactStatus.Rejected.ToString(), Text = ContactStatus.Rejected.Description() });
                list.Add(new SelectListItem { Value = ContactStatus.New.ToString(), Text = ContactStatus.New.Description() });
                return list;
            }
        }

    }
}
