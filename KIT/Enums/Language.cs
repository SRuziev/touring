﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.Enums
{
    public enum Language
    {
        [Description("English")]
        En,

        [Description("Русский")]
        Ru
    }
}
