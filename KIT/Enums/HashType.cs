﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.Enums
{
    public enum HashType
    {
        MD5,
        SHA1,
        SHA256,
        SHA512
    }
}
