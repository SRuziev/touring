﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.Enums
{
    public enum ContactStatus
    {
        [Description("Закрытая/Успех")]
        Success,

        [Description("Закрытая/Отказ")]

        Rejected,

        [Description("Новая")]
        New
    }
}
