﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.Enums
{
    public enum PageSize
    {
        [Description("10")]
        Size10 = 10,
        [Description("20")]
        Size25 = 20,
        [Description("50")]
        Size50 = 50,
        [Description("100")]
        Size100 = 100,
        [Description("200")]
        Size200 = 200
    }
}
