﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace KIT.Enums.Extensions
{
    public static class EnumExtensions
    {
        public static string Description(this System.Enum @enum)
        {
            if (@enum == null) return null;
            var description = @enum.ToString();
            var fieldInfo = @enum.GetType().GetField(description);
            var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Any() ? attributes[0].Description : description;
        }

        public static bool HasAttribute<TEnum>(TEnum value, Type attributeType)
        {
            var fi = value.GetType().GetField(value.ToString());

            var attributes = fi.GetCustomAttributes(attributeType, false);

            return (attributes.Length > 0);
        }

        public static string GetEnumDescription<TEnum>(TEnum value)
        {
            var fi = value.GetType().GetField(value.ToString());

            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
        }

        public static string ToUpperString(this System.Enum eEnum)
        {
            return eEnum.ToString().ToUpper();
        }

        public static string ToLowerString(this System.Enum eEnum)
        {
            return eEnum.ToString().ToLower();
        }

        private static Type GetNonNullableModelType(ModelMetadata modelMetadata)
        {
            Type realModelType = modelMetadata.ModelType;

            Type underlyingType = Nullable.GetUnderlyingType(realModelType);
            if (underlyingType != null)
            {
                realModelType = underlyingType;
            }
            return realModelType;
        }
        private static readonly SelectListItem SingleEmptyItem = new SelectListItem { Text = "", Value = "" };
        public static MvcHtmlString EnumsDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlAttributes, string optionLabel = null)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            Type enumType = GetNonNullableModelType(metadata);
            IEnumerable<TEnum> values = Enum.GetValues(enumType).Cast<TEnum>();

            var items = new List<SelectListItem>();
            foreach (var value in values)
            {


                var selectListItem = new SelectListItem
                {
                    Text = EnumExtensions.GetEnumDescription(value),
                    Value = value.ToString(),
                    Selected = value.Equals(metadata.Model)
                };
                items.Add(selectListItem);
            }

            if (metadata.IsNullableValueType)
                items.Insert(0, SingleEmptyItem);

            return new MvcHtmlString(htmlHelper.DropDownListFor(expression, items, optionLabel, htmlAttributes).ToString());
        }
    }
}
