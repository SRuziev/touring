﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIT.Enums
{
    public enum SortableType
    {
        None = 0,
        Asc = 1,
        Desc = 2
    }
}
