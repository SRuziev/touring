﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using KIT.Enums;
using KIT.Models.BaseModels;
using KIT.Repositories;
using PagedList;


namespace AdminTour.Processors
{
    public abstract class BaseListProcessor<TViewItem, TFilters>
        where TViewItem : new()
        where TFilters : new()
    {
        private int _pageNumber;
        private int _itemsCount;
        private PageSize _pageSize;

        protected readonly ICommonRepository CommonRepository;
        protected TFilters Filters;

        /// <summary>
        /// Название поля [Для сортировки]
        /// </summary>
        protected string SortNameField;
        private SortableType _sortType;

        protected BaseListProcessor(ICommonRepository commonRepository)
        {
            CommonRepository = commonRepository;

            Filters = new TFilters();

            SortNameField = string.Empty;
            _sortType = SortableType.None;

            _pageNumber = 1;
            _pageSize = PageSize.Size10;
        }

        /// <summary>
        /// Инициализация
        /// </summary>
        public void Initialize(ListViewModel<TViewItem, TFilters> listViewModel)
        {
            if (listViewModel == null)
                return;

            _pageNumber = listViewModel.PageInfo.PageNumber;
            _pageSize = listViewModel.PageInfo.PageSize;
            Filters = listViewModel.Filters;
            SortNameField = listViewModel.PageInfo.SortNameField;
            _sortType = listViewModel.PageInfo.SortableType;
        }

        /// <summary>
        /// Разделить по страницам
        /// </summary>
        protected List<TQueryable> Paginate<TQueryable>(IQueryable<TQueryable> queryable)
        {
            _pageNumber = _pageNumber <= 0 ? 1 : _pageNumber;
            var pagedList = queryable.ToPagedList(_pageNumber, (int)_pageSize);
            _itemsCount = pagedList.TotalItemCount;
            return pagedList.ToList();
        }

        /// <summary>
        /// Сортировка
        /// </summary>
        protected IQueryable<TQueryable> Sorting<TQueryable, TSelector>(IQueryable<TQueryable> queryable, Expression<Func<TQueryable, TSelector>> expression)
        {
            return _sortType == SortableType.Asc ? queryable.OrderBy(expression) : _sortType == SortableType.Desc ? queryable.OrderByDescending(expression) : queryable;
        }

        /// <summary>
        /// Фильтрованный список данных. [ Пагинация, фильтрация, сортировка ].
        /// </summary>
        public ListViewModel<TViewItem, TFilters> ToFilteredResult => new ListViewModel<TViewItem, TFilters>
        {
            Items = FilteredList(),
            PageInfo = SetPageInfo(),
            Filters = Filters
        };

        private PageInfo SetPageInfo()
        {
            var newPageInfo = new PageInfo
            {
                PageNumber = _pageNumber,
                PageSize = _pageSize,
                TotalItems = _itemsCount,
                SortNameField = SortNameField,
                SortableType = _sortType
            };

            if (newPageInfo.PageNumber <= newPageInfo.TotalPages)
                return newPageInfo;

            newPageInfo.PageNumber = newPageInfo.TotalPages;
            _pageNumber = newPageInfo.PageNumber;
            return newPageInfo;
        }

        protected abstract List<TViewItem> FilteredList();
    }
}