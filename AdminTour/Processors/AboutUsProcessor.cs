﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Models;
using KIT.Repositories;

namespace AdminTour.Processors
{
    public class AboutUsProcessor:BaseProcessor
    {
        public AboutUsProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        public List<AboutUs> GetText()
        {
            var about = CommonRepository.Entities<AboutUs>();
            return about.ToList();
        }

        public AboutUs GetAboutUs(long id)
        {
            var about = CommonRepository.SingleOrDefault<AboutUs>(w => w.Id == id);
            return about;
        }

        public void ChangeText(AboutUs aboutUs)
        {
            var about = CommonRepository.SingleOrDefault<AboutUs>(w => w.Id == aboutUs.Id);
            about.Text = aboutUs.Text;
            about.Text_en = aboutUs.Text_en;
            CommonRepository.Update(about);
            CommonRepository.SaveChanges();
        }

        public bool IsValidChange(AboutUs model, out Dictionary<string, string> errors)
        {
            var valid = true;
            errors = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(model.Text))
            {
                errors.Add("Text", "Заполните текстовое поле");
                valid = false;
            }

            if (string.IsNullOrEmpty(model.Text_en))
            {
                errors.Add("Text_en", "Заполните текстовое поле");
                valid = false;
            }

            return valid;
        }

    }
}