﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Enums;
using KIT.Helpers;
using KIT.Models;
using KIT.ModelsVm;
using KIT.Repositories;

namespace AdminTour.Processors
{
    public class PersonProcessor:BaseProcessor
    {
        public PersonProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        public AdminAccountVm MyProfile(long id)
        {
            var persones = AccessContext.GetAccount;
            var person = CommonRepository.SingleOrDefault<AdminAccount>(w => w.Id == id);
            return ConvertToAdminVm(person);
        }

        public void ChangeProfile(AdminAccountVm adminAccountVm)
        {
            var person = CommonRepository.SingleOrDefault<AdminAccount>(w => w.Id == adminAccountVm.Id);

            if (!string.IsNullOrEmpty(adminAccountVm.FullName))
                person.FullName = adminAccountVm.FullName;

            if (!string.IsNullOrEmpty(adminAccountVm.NewPassword))
                person.Password = Hash.GetHash(adminAccountVm.NewPassword, HashType.MD5);

            CommonRepository.Update(person);
            CommonRepository.SaveChanges();

        }

        public AdminAccountVm ConvertToAdminVm(AdminAccount adminAccount)
        {
            return new AdminAccountVm
            {
                Id = adminAccount.Id,
                FullName = adminAccount.FullName,
                Login = adminAccount.Login,
                Password = adminAccount.Password
            };
        }

        public bool IsValidProfile(Dictionary<string, string> errors, AdminAccountVm viewModel)
        {
            var valid = true;

            if (string.IsNullOrEmpty(viewModel.FullName))
            {
                errors.Add("FullName", "Введите имя персоны");
                valid = false;
            }

            if (viewModel.NewPassword != viewModel.RepeatPassword)
            {
                errors.Add("RepeatPassword", "Пароли не совпадают");
                valid = false;
            }

            if (!string.IsNullOrEmpty(viewModel.OldPassword))
            {

                if (viewModel.Password != Hash.GetHash(viewModel.OldPassword, HashType.MD5))
                {
                    errors.Add("OldPassword", "Не верный пароль");
                    valid = false;
                }
            }

            if (!string.IsNullOrEmpty(viewModel.OldPassword) && string.IsNullOrEmpty(viewModel.NewPassword))
            {
                errors.Add("NewPassword", "Введите пароль");
                valid = false;
            }

            if (string.IsNullOrEmpty(viewModel.OldPassword) && !string.IsNullOrEmpty(viewModel.NewPassword))
            {
                errors.Add("OldPassword", "Введите пароль");
                valid = false;
            }

            return valid;
        }

    }
}