﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Models;
using KIT.ModelsVm;
using KIT.Repositories;

namespace AdminTour.Processors
{
    public class InverseTourProcessor:BaseProcessor
    {
        public InverseTourProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        public InverseStatusVm GetInverseStatus(long id)
        {
            var inverse = CommonRepository.SingleOrDefault<InverseTour>(w => w.Id == id);
            var inverseVm= new InverseStatusVm();
            inverseVm.id = inverse.Id;
            inverseVm.InverseStatus = inverse.BidStatus;
            return inverseVm;
        }

        public void ChangeStatus(InverseStatusVm inverseStatusVm)
        {
            var inverse = CommonRepository.SingleOrDefault<InverseTour>(w => w.Id == inverseStatusVm.id);
            inverse.BidStatus = inverseStatusVm.InverseStatus;
            CommonRepository.Update(inverse);
            CommonRepository.SaveChanges();
        }

        public InverseInfoTourVm GetInfoInverseTourVm(long id)
        {
            var inverse = CommonRepository.SingleOrDefault<InverseTour>(w => w.Id == id);
            return ConverToInverseInfoTourVm(inverse);
        }

        public void ChangeInverseTour(InverseInfoTourVm inverseInfoTour)
        {
            var inverse = CommonRepository.SingleOrDefault<InverseTour>(w => w.Id == inverseInfoTour.id);
            inverse.FullName = inverseInfoTour.FullName;
            inverse.CityFly = inverseInfoTour.CityFly;
            inverse.DateFly = inverseInfoTour.DateFly;
            inverse.Email = inverseInfoTour.Email;
            inverse.PhoneNumber = inverseInfoTour.PhoneNumber;
            inverse.TourId = inverseInfoTour.TourId;
            inverse.PeopleCount = inverseInfoTour.PeopleCount;
            CommonRepository.Update(inverse);
            CommonRepository.SaveChanges();
        }

        public InverseInfoTourVm ConverToInverseInfoTourVm(InverseTour inverseTour)
        {
            var tour = CommonRepository.SingleOrDefault<Tour>(w => w.Id == inverseTour.TourId);
            return new InverseInfoTourVm
            {
                IpAdress = inverseTour.IpAdress,
                BidStatus = inverseTour.BidStatus,
                NameTour = tour.NameTour,
                id = inverseTour.Id,
                Email = inverseTour.Email,
                CityFly = inverseTour.CityFly,
                DateFly = inverseTour.DateFly,
                PeopleCount = inverseTour.PeopleCount,
                PhoneNumber = inverseTour.PhoneNumber,
                FullName = inverseTour.FullName,
                CreateDate = inverseTour.CreationDateTime,
                TourId = inverseTour.TourId
            };
        }

        public bool IsValidChangeInverse(InverseInfoTourVm viewModel, out Dictionary<string, string> errors)
        {
            var valid = true;
            errors = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(viewModel.CityFly))
            {
                errors.Add("CityFly", "Укажите Страну/Город вылета");
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.Email))
            {
                errors.Add("Email", "Укажите почту");
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.PhoneNumber))
            {
                errors.Add("PhoneNumber", "Укажите номер телефона");
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.FullName))
            {
                errors.Add("FullName", "Укажите ФИО");
                valid = false;
            }
            if (viewModel.PeopleCount==0)
            {
                errors.Add("PeopleCount", "Укажите количество людей");
                valid = false;
            }

            return valid;
        }
    }
}