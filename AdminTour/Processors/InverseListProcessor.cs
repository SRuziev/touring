﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Models;
using KIT.ModelsVm;
using KIT.Repositories;

namespace AdminTour.Processors
{
    public class InverseListProcessor:BaseListProcessor<InverseContactVm,InverseFilter>
    {
        public InverseListProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        protected override List<InverseContactVm> FilteredList()
        {
            var query = CommonRepository.Entities<InverseContact>();

            query = query.Where(w => w.CreationDateTime >= Filters.DateStart &&
                                     w.CreationDateTime <= Filters.DateEnd);

            if (Filters.BidStatus != null)
                query = query.Where(w => w.BidStatus == Filters.BidStatus.Value);

            if (!string.IsNullOrEmpty(Filters.Email))
                query = query.Where(w => w.Email.ToLower().Contains(Filters.Email.ToLower()) || Filters.Email.ToLower().Contains(w.Email.ToLower()));

            if (!string.IsNullOrEmpty(Filters.FullName))
                query = query.Where(w => w.FullName.ToLower().Contains(Filters.FullName.ToLower()) || Filters.FullName.ToLower().Contains(w.FullName.ToLower()));

            query = query.OrderBy(w => w.CreationDateTime);

            switch (SortNameField)
            {
                case "BidStatus":
                    query = Sorting(query, w => w.BidStatus);
                    break;
                case "CreateDate":
                    query = Sorting(query, w => w.CreationDateTime);
                    break;
                case "Id":
                    query = Sorting(query, w => w.Id);
                    break;
                case "FullName":
                    query = Sorting(query, w => w.FullName);
                    break;
                case "Email":
                    query = Sorting(query, w => w.Email);
                    break;
                case "Number":
                    query = Sorting(query, w => w.Number);
                    break;
                case "IpAdress":
                    query = Sorting(query, w => w.IpAdress);
                    break;
            }

            return Paginate(query).Select(GetVmList).ToList();
        }

        public InverseContactVm GetVmList(InverseContact inverseContact)
        {
            return new InverseContactVm
            {
                Id = inverseContact.Id,
                BidStatus = inverseContact.BidStatus,
                CreateDate = inverseContact.CreationDateTime,
                Email = inverseContact.Email,
                FullName = inverseContact.FullName,
                IpAdress = inverseContact.IpAdress,
                Number = inverseContact.Number
            };
        }
    }
}