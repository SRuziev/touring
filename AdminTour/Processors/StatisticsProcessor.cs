﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Enums;
using KIT.Models;
using KIT.ModelsVm;
using KIT.Repositories;

namespace AdminTour.Processors
{
    public class StatisticsProcessor:BaseProcessor
    {
        public StatisticsProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        public StatisticsVm GetStatisticsVm()
        {
            var newInverseTour = 0;
            var succsuesInverseTour = 0;
            var rejectedInverseTour = 0;

            var newInverseContact = 0;
            var succsuesInverseContact = 0;
            var rejectedInverseContact = 0;
            var invetseTour = CommonRepository.Entities<InverseTour>().ToList();
            var invetseContact = CommonRepository.Entities<InverseContact>().ToList();
            foreach (var inverse in invetseTour)
            {
                if (inverse.BidStatus == ContactStatus.New)
                    newInverseTour += 1;
                if (inverse.BidStatus == ContactStatus.Rejected)
                    rejectedInverseTour += 1;
                if (inverse.BidStatus == ContactStatus.Success)
                    succsuesInverseTour += 1;
            }
            foreach (var inverse in invetseContact)
            {
                if (inverse.BidStatus == ContactStatus.New)
                    newInverseContact += 1;
                if (inverse.BidStatus == ContactStatus.Rejected)
                    rejectedInverseContact += 1;
                if (inverse.BidStatus == ContactStatus.Success)
                    succsuesInverseContact += 1;
            }
            return new StatisticsVm
            {
                NewInverseContact = newInverseContact,
                NewInverseTour = newInverseTour,
                RejectedInverseContact = rejectedInverseContact,
                RejectedInverseTour = rejectedInverseTour,
                SuccsuesInverseContact = succsuesInverseContact,
                SuccsuesInverseTour = succsuesInverseTour
            };
        }
    }
}