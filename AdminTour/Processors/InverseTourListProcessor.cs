﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Models;
using KIT.ModelsVm;
using KIT.Repositories;

namespace AdminTour.Processors
{
    public class InverseTourListProcessor:BaseListProcessor<InverseTourVm,InverseTourFilter>
    {
        public InverseTourListProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        protected override List<InverseTourVm> FilteredList()
        {
            var query =
                from inverse in CommonRepository.Entities<InverseTour>()
                join tour in CommonRepository.Entities<Tour>()
                on inverse.TourId equals tour.Id

                select new
                {
                    InverseTour = inverse,
                    Tour = tour
                };

            query = query.Where(w => w.InverseTour.CreationDateTime >= Filters.DateStart &&
                                     w.InverseTour.CreationDateTime <= Filters.DateEnd);

            if (Filters.BidStatus != null)
                query = query.Where(w => w.InverseTour.BidStatus == Filters.BidStatus.Value);

            if (!string.IsNullOrEmpty(Filters.FullName))
                query = query.Where(w => w.InverseTour.FullName.ToLower().Contains(Filters.FullName.ToLower()) || Filters.FullName.ToLower().Contains(w.InverseTour.FullName.ToLower()));

            if (Filters.Tour.HasValue)
                query = query.Where(w => w.InverseTour.TourId == Filters.Tour);


            query = query.OrderBy(w => w.InverseTour.CreationDateTime);

            switch (SortNameField)
            {
                case "BidStatus":
                    query = Sorting(query, w => w.InverseTour.BidStatus);
                    break;
                case "CreateDate":
                    query = Sorting(query, w => w.InverseTour.CreationDateTime);
                    break;
                case "Id":
                    query = Sorting(query, w => w.InverseTour.Id);
                    break;
                case "FullName":
                    query = Sorting(query, w => w.InverseTour.FullName);
                    break;
                case "PeopleCount":
                    query = Sorting(query, w => w.InverseTour.PeopleCount);
                    break;
                
                case "TourName":
                    query = Sorting(query, w => w.Tour.NameTour);
                    break;

            }

            var queryable = query.Select(w => w.InverseTour);
            return Paginate(queryable).Select(GetVmList).ToList();
        }
        public InverseTourVm GetVmList(InverseTour inverseTour)
        {
            var tour = CommonRepository.SingleOrDefault<Tour>(w => w.Id == inverseTour.TourId);
            return new InverseTourVm
            {
                id = inverseTour.Id,
                BidStatus = inverseTour.BidStatus,
                CreateDate = inverseTour.CreationDateTime,
                FullName = inverseTour.FullName,
                TourName = tour.NameTour,
            };
        }
    }
}