﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Models;
using KIT.ModelsVm;
using KIT.Repositories;

namespace AdminTour.Processors
{
    public class VideoProcessor:BaseProcessor
    {
        public VideoProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        public List<Video> GetAll()
        {
            var video = CommonRepository.Entities<Video>().ToList();
            return video;
        }

        public void Delete(long id)
        {
            var video = CommonRepository.SingleOrDefault<Video>(w => w.Id == id);
            CommonRepository.Remove(video);
            CommonRepository.SaveChanges();
        }

        public Video GetVideo(long id)
        {
            var video = CommonRepository.SingleOrDefault<Video>(w => w.Id == id);
            return video;
        }

        public void ChangeVideo(Video video)
        {
            var videos = CommonRepository.SingleOrDefault<Video>(w => w.Id == video.Id);
            videos.Name = video.Name;
            videos.Name_en = video.Name_en;
            videos.Url = video.Url;
            CommonRepository.Update(videos);
            CommonRepository.SaveChanges();
        }

        public void AddVideo(VideoVm videoVm)
        {
            var video= new Video();
            video.Name = videoVm.Name;
            video.Name_en = videoVm.Name_en;
            video.Url = videoVm.Url;
            CommonRepository.Add(video);
            CommonRepository.SaveChanges();
        }

        public bool IsValid(Video model, out Dictionary<string, string> errors)
        {
            var valid = true;
            errors = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(model.Name))
            {
                errors.Add("Name", "Укажите название видео");
                valid = false;
            }

            if (string.IsNullOrEmpty(model.Name_en))
            {
                errors.Add("Name_en", "Укажите название видео(Английский)");
                valid = false;
            }

            if (string.IsNullOrEmpty(model.Url))
            {
                errors.Add("Url", "Укажите адрес видео");
                valid = false;
            }

            return valid;
        }

        public bool IsValid(VideoVm model, out Dictionary<string, string> errors)
        {
            var valid = true;
            errors = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(model.Name))
            {
                errors.Add("Name", "Укажите название видео");
                valid = false;
            }

            if (string.IsNullOrEmpty(model.Name_en))
            {
                errors.Add("Name_en", "Укажите название видео(Английский)");
                valid = false;
            }

            if (string.IsNullOrEmpty(model.Url))
            {
                errors.Add("Url", "Укажите адрес видео");
                valid = false;
            }

            return valid;
        }

    }
}