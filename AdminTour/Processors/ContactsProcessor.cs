﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Models;
using KIT.ModelsVm;
using KIT.Repositories;

namespace AdminTour.Processors
{
    public class ContactsProcessor:BaseProcessor
    {
        public ContactsProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        public List<ContactsVm> GetContacts()
        {
            var contacts = CommonRepository.Entities<Contacts>();
            return contacts.ToList().Select(ConvertToVm).ToList();
        }

        public ContactsVm GetContactsVm(long? id)
        {
            var contact = CommonRepository.Entities<Contacts>().SingleOrDefault();
            return ConvertToVm(contact);
        }

        public void ChangeContacts(ContactsVm contactsVm)
        {
            var contact = CommonRepository.SingleOrDefault<Contacts>(w => w.Id == contactsVm.Id);
            contact.Email = contactsVm.Email;
            contact.PhoneNumber = contactsVm.PhoneNumber;
            contact.Fb = contactsVm.Fb;
            contact.Vk = contactsVm.Vk;
            contact.Instagramm = contactsVm.Instagramm;
            contact.Youtube = contactsVm.Youtube;
            CommonRepository.Update(contact);
            CommonRepository.SaveChanges();
        }

        public ContactsVm ConvertToVm(Contacts contacts)
        {
            return new ContactsVm
            {
                PhoneNumber = contacts.PhoneNumber,
                Email = contacts.Email,
                Fb = contacts.Fb,
                Instagramm = contacts.Instagramm,
                Vk = contacts.Vk,
                Id = contacts.Id,
                Youtube = contacts.Youtube
            };
        }

        public bool IsValidContacts(ContactsVm viewModel, out Dictionary<string, string> errors)
        {
            var valid = true;
            errors = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(viewModel.Email))
            {
                errors.Add("Email", "Укажите Email");
                valid = false;
            }

            if (string.IsNullOrEmpty(viewModel.PhoneNumber))
            {
                errors.Add("PhoneNumber", "Укажите номер телефона");
                valid = false;
            }

            if (string.IsNullOrEmpty(viewModel.Fb))
            {
                errors.Add("Fb", "Укажите ссылку на страницу в Facebook");
                valid = false;
            }

            if (string.IsNullOrEmpty(viewModel.Instagramm))
            {
                errors.Add("Instagramm", "Укажите ссылку на страницу в Instagramm");
                valid = false;
            }

            if (string.IsNullOrEmpty(viewModel.Vk))
            {
                errors.Add("Vk", "Укажите ссылку на страницу в VK");
                valid = false;
            }

            if (string.IsNullOrEmpty(viewModel.Youtube))
            {
                errors.Add("Youtube", "Укажите ссылку на канал в Youtube");
                valid = false;
            }

            return valid;
        }
    }
}