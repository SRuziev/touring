﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Models;
using KIT.ModelsVm;
using KIT.Repositories;

namespace AdminTour.Processors
{
    public class IssuesProcessor:BaseProcessor
    {
        public IssuesProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        public List<IssueTourVm> AllIssuesClient()
        {
            var issues = CommonRepository.Entities<IssuesClient>();
            return issues.ToList().Select(ConvertToVm).ToList();
        }

        public void DeleteIssues(long? id)
        {
            var issues = CommonRepository.SingleOrDefault<IssuesClient>(w => w.Id == id);
            CommonRepository.Remove(issues);
            CommonRepository.SaveChanges();
        }

        public void AddIssues(IssueTourVm issuesClientVm)
        {
            CommonRepository.Add(ConvertToDb(issuesClientVm));
            CommonRepository.SaveChanges();
        }

        public IssueTourVm GetIssuesClientVm(long? id)
        {
            var issues = CommonRepository.SingleOrDefault<IssuesClient>(w => w.Id == id);
            return ConvertToVm(issues);
        }

        public void ChangeIssues(IssueTourVm issuesClientVm)
        {
            var issues = CommonRepository.SingleOrDefault<IssuesClient>(w => w.Id == issuesClientVm.Id);
            issues.Answer = issuesClientVm.Answer;
            issues.Answer_en = issuesClientVm.Answer_en;
            issues.Issues = issuesClientVm.Issues;
            issues.Issues_en = issuesClientVm.Issues_en;
            CommonRepository.Update(issues);
            CommonRepository.SaveChanges();
        }

        public IssueTourVm ConvertToVm(IssuesClient issuesClient)
        {
            return new IssueTourVm
            {
                Issues = issuesClient.Issues,
                Issues_en = issuesClient.Issues_en,
                Answer = issuesClient.Answer,
                Answer_en = issuesClient.Answer_en,
                Id = issuesClient.Id

            };
        }

        

        public IssuesClient ConvertToDb(IssueTourVm issuesClientVm)
        {
            return new IssuesClient
            {
                Issues = issuesClientVm.Issues,
                Issues_en = issuesClientVm.Issues_en,
                Answer = issuesClientVm.Answer,
                Answer_en = issuesClientVm.Answer_en
            };
        }

        public bool IsValidIssues(IssueTourVm viewModel, out Dictionary<string, string> errors)
        {
            var valid = true;
            errors = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(viewModel.Issues))
            {
                errors.Add("Issues", "Укажите вопрос");
                valid = false;
            }

            if (string.IsNullOrEmpty(viewModel.Issues_en))
            {
                errors.Add("Issues_en", "Укажите вопрос");
                valid = false;
            }

            if (string.IsNullOrEmpty(viewModel.Answer))
            {
                errors.Add("Answer", "Укажите ответ");
                valid = false;
            }

            if (string.IsNullOrEmpty(viewModel.Answer_en))
            {
                errors.Add("Answer_en", "Укажите ответ");
                valid = false;
            }

            return valid;
        }
    }
}