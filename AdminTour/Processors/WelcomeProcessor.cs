﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Models;
using KIT.Repositories;

namespace AdminTour.Processors
{
    public class WelcomeProcessor:BaseProcessor
    {
        public WelcomeProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        public List<Welcome> GetText()
        {
            var welcome = CommonRepository.Entities<Welcome>();
            return welcome.ToList();
        }

        public Welcome GetWelcome(long id)
        {
            var welcome = CommonRepository.SingleOrDefault<Welcome>(w => w.Id == id);
            return welcome;
        }

        public void ChangeText(Welcome welcome)
        {
            var welcoms = CommonRepository.SingleOrDefault<Welcome>(w => w.Id == welcome.Id);
            welcoms.Text = welcome.Text;
            welcoms.Text_en = welcome.Text_en;
            CommonRepository.Update(welcoms);
            CommonRepository.SaveChanges();
        }

        public bool IsValidChange(Welcome model, out Dictionary<string, string> errors)
        {
            var valid = true;
            errors = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(model.Text))
            {
                errors.Add("Text", "Заполните текстовое поле");
                valid = false;
            }

            if (string.IsNullOrEmpty(model.Text_en))
            {
                errors.Add("Text_en", "Заполните текстовое поле");
                valid = false;
            }

            return valid;
        }
    }
}