﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Enums;
using KIT.Helpers;
using KIT.Models;
using KIT.ModelsVm;
using KIT.Repositories;

namespace AdminTour.Processors
{
    public class SecurityProcessor:BaseProcessor
    {
        public SecurityProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        public List<SecurityVm> AllIp()
        {
            var ip = CommonRepository.Entities<Security>();
            return ip.ToList().Select(ConvertToVM).ToList();
        }

        public void DeleteIp(long id)
        {
            var ip = CommonRepository.SingleOrDefault<Security>(w => w.Id == id);
            CommonRepository.Remove(ip);
            CommonRepository.SaveChanges();
        }

        public void AddIp(SecurityVm securityVm)
        {
            var model = ConvertToDb(securityVm);
            CommonRepository.Add(model);
            CommonRepository.SaveChanges();
        }

        public bool IsValidProfile(SecurityVm viewModel, out Dictionary<string, string> errors)
        {
            var valid = true;
            errors = new Dictionary<string, string>();
            var ip = CommonRepository.SingleOrDefault<Security>(w => w.Ip == viewModel.Ip);
            if (string.IsNullOrEmpty(viewModel.Ip))
            {
                errors.Add("Ip", "Укажите IP адрес");
                valid = false;
            }

            if (ip!=null)
            {
                errors.Add("Ip", "Данный IP адрес уже существует");
                valid = false;
            }


            return valid;
        }

        public SecurityVm ConvertToVM(Security domainModel)
        {
            return new SecurityVm
            {
                Ip = domainModel.Ip,
                Comment = domainModel.Comment,
                id = domainModel.Id
            };
        }

        public Security ConvertToDb(SecurityVm vmModel)
        {
            return new Security
            {
                Ip = vmModel.Ip,
                Comment = vmModel.Comment,
            };
        }
    }
}