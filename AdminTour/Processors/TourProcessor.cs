﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using KIT.Models;
using KIT.ModelsVm;
using KIT.Repositories;

namespace AdminTour.Processors
{
    public class TourProcessor:BaseProcessor
    {
        public TourProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        public List<TourTableVm> AllTour()
        {
            var tour = CommonRepository.Entities<Tour>().Where(w=>w.IsDeleted==false);
            return tour.ToList().Select(ConverToTourTableVm).ToList();
        }

        public void LogicDelete(long id)
        {
            var tour = CommonRepository.SingleOrDefault<Tour>(w => w.Id == id);
            tour.IsDeleted = true;
            CommonRepository.Update(tour);
            CommonRepository.SaveChanges();
        }

        public void AddTour(TourVm tourVm)
        {
            var tour= new Tour();
            tour.IsDeleted = false;
            tour.NameTour = tourVm.NameTour;
            tour.NameTour_en = tourVm.NameTour_en;
            tour.Description = tourVm.Description;
            tour.Description_en = tourVm.Description_en;
            tour.ShortDescription = tourVm.ShortDescription;
            tour.ShortDescription_en = tourVm.ShortDescription_en;
            
            if (tourVm.File.Base != null && tourVm.File.Base.ContentLength > 0)
            {
                tour.UploadedFile = new UploadedFile
                {
                    ContentType = tourVm.File.Base.ContentType,
                    FileName = tourVm.File.Base.FileName
                };

                using (var reader = new BinaryReader(tourVm.File.Base.InputStream))
                {
                    tour.UploadedFile.File = reader.ReadBytes(tourVm.File.Base.ContentLength);
                }
                
            }
            CommonRepository.Add(tour);
            CommonRepository.SaveChanges();
        }

        public TourVm TourInfo(long? id)
        {
            var tour = CommonRepository.SingleOrDefault<Tour>(w => w.Id == id);
            return ConvertToTourVm(tour);
        }

        public void ChangeTour(TourVm tourVm)
        {
            var tour = CommonRepository.SingleOrDefault<Tour>(w => w.Id == tourVm.Id);
            tour.NameTour = tourVm.NameTour;
            tour.NameTour_en = tourVm.NameTour_en;
            tour.Description = tourVm.Description;
            tour.Description_en = tourVm.Description_en;
            tour.ShortDescription = tourVm.ShortDescription;
            tour.ShortDescription_en = tourVm.ShortDescription_en;

            if (tourVm.File.Base != null && tourVm.File.Base.ContentLength > 0)
            {
                tour.UploadedFile = new UploadedFile
                {
                    ContentType = tourVm.File.Base.ContentType,
                    FileName = tourVm.File.Base.FileName
                };

                using (var reader = new BinaryReader(tourVm.File.Base.InputStream))
                {
                    tour.UploadedFile.File = reader.ReadBytes(tourVm.File.Base.ContentLength);
                }
            }
            CommonRepository.Update(tour);
            CommonRepository.SaveChanges();
        }

        public TourVm ConvertToTourVm(Tour tour)
        {
            var uploadedFile = new UploadedFileVm
            {
                FileName = tour.UploadedFile.FileName,
                ContentType = tour.UploadedFile.ContentType,
                Bytes = tour.UploadedFile.File
            };

            return new TourVm
            {
               File = uploadedFile,
               Id = tour.Id,
               NameTour = tour.NameTour,
               Description = tour.Description,
               ShortDescription = tour.ShortDescription,
               NameTour_en = tour.NameTour_en,
               Description_en = tour.Description_en,
               ShortDescription_en = tour.ShortDescription_en
            };
        }

        public TourTableVm ConverToTourTableVm(Tour tour)
        {
            return new TourTableVm
            {
                Id = tour.Id,
                NameTour = tour.NameTour
            };
        }

        public bool IsValidAddTour(TourVm viewModel, out Dictionary<string, string> errors)
        {
            var valid = true;
            errors = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(viewModel.NameTour))
            {
                errors.Add("NameTour", "Укажите название тура");
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.NameTour_en))
            {
                errors.Add("NameTour_en", "Укажите название тура");
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.ShortDescription))
            {
                errors.Add("ShortDescription", "Заполните краткое описание тура");
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.ShortDescription_en))
            {
                errors.Add("ShortDescription_en", "Заполните краткое описание тура");
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.Description))
            {
                errors.Add("Description", "Заполните описание тура");
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.Description_en))
            {
                errors.Add("Description_en", "Заполните описание тура");
                valid = false;
            }
            if (viewModel.File.Base == null || viewModel.File.Base.ContentLength <= 0)
            {
                errors.Add("FileForUpload", "Выберите изображение");
                valid = false;
            }

                return valid;
        }
        public bool IsValidChangeTour(TourVm viewModel, out Dictionary<string, string> errors)
        {
            var valid = true;
            errors = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(viewModel.NameTour))
            {
                errors.Add("NameTour", "Укажите название тура");
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.NameTour_en))
            {
                errors.Add("NameTour_en", "Укажите название тура");
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.ShortDescription))
            {
                errors.Add("ShortDescription", "Заполните краткое описание тура");
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.ShortDescription_en))
            {
                errors.Add("ShortDescription_en", "Заполните краткое описание тура");
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.Description))
            {
                errors.Add("Description", "Заполните описание тура");
                valid = false;
            }
            if (string.IsNullOrEmpty(viewModel.Description_en))
            {
                errors.Add("Description_en", "Заполните описание тура");
                valid = false;
            }
            

            return valid;
        }
    }
}