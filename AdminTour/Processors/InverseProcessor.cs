﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KIT.Helpers;
using KIT.Models;
using KIT.ModelsVm;
using KIT.Repositories;

namespace AdminTour.Processors
{
    public class InverseProcessor:BaseProcessor
    {
        public InverseProcessor(ICommonRepository commonRepository) : base(commonRepository)
        {
        }

        public InverseStatusVm InverseStatus(long id)
        {
            var inverse = CommonRepository.SingleOrDefault<InverseContact>(w => w.Id == id);
            return new InverseStatusVm
            {
                id = inverse.Id,
                InverseStatus = inverse.BidStatus
            };
        }

        public void ChangeStatus(InverseStatusVm inverseStatusVm)
        {
            var inverse = CommonRepository.SingleOrDefault<InverseContact>(w => w.Id == inverseStatusVm.id);
            inverse.BidStatus = inverseStatusVm.InverseStatus;
            CommonRepository.Update(inverse);
            CommonRepository.SaveChanges();
        }
    }
}