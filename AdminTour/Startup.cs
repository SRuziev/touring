﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AdminTour.Startup))]
namespace AdminTour
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
