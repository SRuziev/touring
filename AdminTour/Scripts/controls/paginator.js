﻿Paginator = new function () {
    this.SetPageNumber = function (pageNumber) {
        $("#PageInfo_PageNumber").val(pageNumber);
        Paginator.SUBMIT();
    }
    this.SUBMIT = function () {
        var form = $("#PageInfo_PageNumber").parent("form[id='filter-table']");
        form.submit();
    }
    this.SetNameForSortField = function (obj) {
        var typeSort = "";
        var fieldName = $(obj).attr('data-field');

        switch ($(obj).attr('data-sort')) {
            case "None":
                typeSort = "Desc";
                break;
            case "Desc":
                typeSort = "Asc";
                break;
            case "Asc":
                typeSort = "None";
                break;
        }

        $("#PageInfo_SortNameField").val(fieldName);
        $("#PageInfo_SortableType").val(typeSort);
    }
}