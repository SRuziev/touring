﻿SelectValueCustom = new function () {
    this.sendAttr = function (e) {
        var ids = $(e).attr('data-for-select');
        var value = $(e).attr('data-for-select-value');
        if (value !== "null") {
            $('select#' + ids + ' option[value="' + value + '"]').prop("selected", true);
        } else {
            $('select#' + ids).children().removeAttr("selected");
            $('select#' + ids + ' :nth-child(1)').prop('selected', true);
        }
        $('button[data-for-select-button="' + ids + '"]').html($(e).text() + " <span class='caret'></span>");
        $('input[data-for-select-button="' + ids + '"]').val($(e).text());
    };
    this.Subscribe = function (id) {
        $('button[data-for-select-button="' + id + '"]').click(function (e) {
            $('ul[data-for-select-list="' + id + '"]').toggleClass("active");
            e.stopPropagation();
        });
    };
};

$(document).ready(function () {
    $('header, #body').click(function (e) {
        if ($('ul[data-for-select-list]').hasClass('active')) {
            $('ul[data-for-select-list]').removeClass('active');
            e.stopPropagation();
        }
    });
});