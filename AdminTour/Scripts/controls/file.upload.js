﻿$(function () {
    var wrapper = $(".file_upload"),
        inp = wrapper.find("input"),
        btn = wrapper.find("button"),
        lbl = wrapper.find("div");

    btn.focus(function () {
        inp.focus();
    });
    
    inp.focus(function () {
        wrapper.addClass("focus");
    }).blur(function () {
        wrapper.removeClass("focus");
    });

    var fileApi = (window.File && window.FileReader && window.FileList && window.Blob) ? true : false;

    inp.change(function () {
        var fileName;
        if (fileApi && inp[0].files[0])
            fileName = inp[0].files[0].name;
        else
            fileName = inp.val().replace("C:\\fakepath\\", "");

        if (!fileName.length)
            return;

        if (lbl.is(":visible")) {
            lbl.text(fileName);
            btn.html('<i class="fa fa-folder-open" aria-hidden="true"></i>');
        } else
            btn.html('<i class="fa fa-folder-open" aria-hidden="true"></i>');
    }).change();

});
$(window).resize(function () {
    $(".file_upload input").triggerHandler("change");
});
