﻿CultureInfo = new function () {
    this.getActive = function () {
        var lang = $.cookie("lang");
        if (lang === undefined)
            return "ru";

        if (lang === "de")
            return "ru";

        return $.cookie("lang");
    }
}