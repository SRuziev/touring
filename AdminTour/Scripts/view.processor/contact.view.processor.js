﻿"use strict";
class ContactViewProcessor {
    constructor(config) {
        this._config = config;
    }

    // private
    FindByName(name) {
        return $(`[let="${name}"]`);
    }

    TryValue(object) {
        if (object == undefined || object === NaN || object.length === 0)
            return "";

        return object.val();
    }

    // private
    BindClick(name, callback) {
        this.FindByName(name).unbind("click").on("click", callback);
    }

    // private
    ShowModal(title, html, callback) {
        var config = this._config;
        bootbox.dialog({
            size: "large",
            title: title,
            message: html,
            backdrop: true,
            animate: true,
            onEscape: true,
            buttons: {
                save: {
                    label: `<i class="fa fa-check"></i> ${config.resource.save}`,
                    className: "btn btn-sm btn-success text-uppercase",
                    callback: callback
                },
                cancel: {
                    label: `<i class="fa fa-ban"></i> ${config.resource.cancel}`,
                    className: "btn btn-sm btn-danger text-uppercase",
                    callback: function () {
                        bootbox.hideAll();
                    }
                }
            }
        });
    }

 

    HideModelErrors() {
        $("[valid-for]").html("");
    }

    ShowModelErrors(errors, formName) {
        var form = this.FindByName(formName);
        $.each(errors, function (key) {
            form.find(`[valid-for='${key}']`).html(errors[key]);
        });
    }

    Bindings() {
        var processor = this;
        this.BindClick("change-contact", function () { processor.ChangeContact($(this).attr("rid")) });
    }

    RenderTable() {
        var processor = this;

        MaskHelper.ShowBlockUi();
        $.get(this._config.url.getAll).done((data) => {
            MaskHelper.HideBlockUI();
            processor.FindByName("contact-table").html(data.html);
            processor.Bindings();
        });
    }

    ChangeContact(id) {
        var processor = this;
        var config = this._config;

        MaskHelper.ShowBlockUi();
        $.get(config.url.change, { id: id }).done((data) => {
            MaskHelper.HideBlockUI();
            processor.ShowModal(config.resource.changeTitle, data.html, function () {
                processor.ChangeIssuesSubmit(this);
                return false;
            });
        });
    }

    ChangeIssuesSubmit(btn) {
        $(btn).find("i").attr("class", "fa fa-spin fa-spinner");
        var processor = this;
        this.HideModelErrors();

        var model = this.FindByName("change-contact-form").serialize();

        MaskHelper.ShowBlockUi();
        $.ajax({ url: this._config.url.change, type: "POST", data: model }).done((data) => {
            $(btn).find("i").attr("class", "fa fa-check");
            MaskHelper.HideBlockUI();
            if (data.status) {
                processor.RenderTable();
                bootbox.hideAll();
                CommonHelper.attention(processor._config.resource.operationComplete, processor._config.resource.changeIsComplete);
            } else {
                processor.ShowModelErrors(data.errors, "change-contact-form");
            }
        });
    }


    Start() {
        this.RenderTable();
        this.Bindings();
    }
}