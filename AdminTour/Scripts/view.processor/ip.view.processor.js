﻿"use strict";
class IpViewProcessor {
    constructor(config) {
        this._config = config;
    }

    // private
    FindByName(name) {
        return $(`[let="${name}"]`);
    }

    // private
    BindClick(name, callback) {
        this.FindByName(name).unbind("click").on("click", callback);
    }

    // private
    ShowModal(title, html, callback) {
        var config = this._config;
        bootbox.dialog({
            size: "large",
            title: title,
            message: html,
            backdrop: true,
            animate: true,
            onEscape: true,
            buttons: {
                save: {
                    label: `<i class="fa fa-check"></i> ${config.resource.save}`,
                    className: "btn btn-sm btn-success text-uppercase",
                    callback: callback
                },
                cancel: {
                    label: `<i class="fa fa-ban"></i> ${config.resource.cancel}`,
                    className: "btn btn-sm btn-danger text-uppercase",
                    callback: function () {
                        bootbox.hideAll();
                    }
                }
            }
        });
    }

    HideModelErrors() {
        $("[valid-for]").html("");
    }

    ShowModelErrors(errors, formName) {
        var form = this.FindByName(formName);
        $.each(errors, function (key) {
            form.find(`[valid-for='${key}']`).html(errors[key]);
        });
    }

    Bindings() {
        var processor = this;
        this.BindClick("delete-ip", function () { processor.DeleteIp($(this).attr("rid")) });
        this.BindClick("add-ip", function () { processor.AddNew() });
    }

    RenderTable() {
        var processor = this;

        MaskHelper.ShowBlockUi();
        $.get(this._config.url.getAll).done((data) => {
            MaskHelper.HideBlockUI();
            processor.FindByName("ip-table").html(data.html);
            processor.Bindings();
        });
    }

    AddNew() {
        var processor = this;
        var config = this._config;

        MaskHelper.ShowBlockUi();
        $.get(config.url.add).done((data) => {
            MaskHelper.HideBlockUI();
            processor.ShowModal(config.resource.addTitle, data.html, function () {
                processor.AddNewSubmit(this);
                return false;
            });
        });
    }

    AddNewSubmit(btn) {
        $(btn).find("i").attr("class", "fa fa-spin fa-spinner");
        var processor = this;
        this.HideModelErrors();
        var model = this.FindByName("add-ip-form").serialize();

        MaskHelper.ShowBlockUi();
        $.ajax({ url: this._config.url.add, type: "POST", data: model }).done((data) => {
            $(btn).find("i").attr("class", "fa fa-check");
            MaskHelper.HideBlockUI();
            if (data.status) {
                processor.RenderTable();
                bootbox.hideAll();
                CommonHelper.attention(processor._config.resource.operationComplete, processor._config.resource.addIsComplete);
            } else {
                processor.ShowModelErrors(data.errors, "add-ip-form");
            }
        });
    }


    DeleteIp(id) {
        var processor = this;
        bootbox.dialog({
            size: "small",
            message: processor._config.resource.deleteTitle,
            backdrop: true,
            animate: true,
            onEscape: true,
            buttons: {
                save: {
                    label: `<i class="fa fa-check"></i> ${processor._config.resource.yes}`,
                    className: "btn btn-sm btn-success text-uppercase",
                    callback: function () {
                        processor.DeletePersonSubmit(id);
                    }
                },
                cancel: {
                    label: `<i class="fa fa-ban"></i> ${processor._config.resource.no}`,
                    className: "btn btn-sm btn-danger text-uppercase",
                    callback: function () {
                        bootbox.hideAll();
                    }
                }
            }
        });
    }

    DeletePersonSubmit(id) {
        var processor = this;

        MaskHelper.ShowBlockUi();
        $.get(processor._config.url.delete, { id: id }).done((data) => {
            MaskHelper.HideBlockUI();
            if (data.status) {
                processor.RenderTable();
                CommonHelper.attention(processor._config.resource.operationComplete, processor._config.resource.deleteIsComplete);
            }
        });
    }

    Start() {
        this.RenderTable();
        this.Bindings();
    }
}