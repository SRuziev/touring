﻿"use strict";

class InverseTourViewProcessor {
    constructor(config) {
        this._config = config;
    }

    FindByName(name) {
        return $(`[let="${name}"]`);
    }

    // private
    BindClick(name, callback) {
        this.FindByName(name).unbind("click").on("click", callback);
    }

    ShowModalInfo(title, html) {
        var config = this._config;
        bootbox.dialog({
            size: "large",
            title: title,
            message: html,
            backdrop: true,
            animate: true,
            onEscape: true,
            buttons: {
                cancel: {
                    label: `<i class="fa fa-close"></i> ${config.resource.close}`,
                    className: "btn btn-sm btn-danger text-uppercase",
                    callback: function () {
                        bootbox.hideAll();
                    }
                }
            }
        });
    }

    ShowModalChange(title, html, callback) {
        var config = this._config;
        bootbox.dialog({
            title: title,
            message: html,
            backdrop: true,
            animate: true,
            onEscape: true,
            buttons: {
                save: {
                    label: `<i class="fa fa-check"></i> ${config.resource.save}`,
                    className: "btn btn-sm btn-success text-uppercase",
                    callback: callback
                },
                cancel: {
                    label: `<i class="fa fa-ban"></i> ${config.resource.cancel}`,
                    className: "btn btn-sm btn-danger text-uppercase",
                    callback: function () {
                        bootbox.hideAll();
                    }
                }
            }
        });
    }

    ShowModal(title, html, callback) {
        var config = this._config;
        bootbox.dialog({
            size: "large",
            title: title,
            message: html,
            backdrop: true,
            animate: true,
            onEscape: true,
            buttons: {
                save: {
                    label: `<i class="fa fa-check"></i> ${config.resource.save}`,
                    className: "btn btn-sm btn-success text-uppercase",
                    callback: callback
                },
                cancel: {
                    label: `<i class="fa fa-ban"></i> ${config.resource.cancel}`,
                    className: "btn btn-sm btn-danger text-uppercase",
                    callback: function () {
                        bootbox.hideAll();
                    }
                }
            }
        });
    }

    HideModelErrors() {
        $("[valid-for]").html("");
    }

    ShowModelErrors(errors, formName) {
        var form = this.FindByName(formName);
        $.each(errors, function (key) {
            form.find(`[valid-for='${key}']`).html(errors[key]);
        });
    }

    Bindings() {
        var processor = this;
        this.BindClick("change-status", function () { processor.ChangeStatus($(this).attr("rid")) });
        this.BindClick("change-inverse", function () { processor.ChangeInverse($(this).attr("rid")) });
        this.BindClick("info-inverse", function () { processor.Info($(this).attr("rid")) });
    }

    Info(id) {
        var processor = this;
        var config = this._config;

        MaskHelper.ShowBlockUi();
        $.get(config.url.info, { id: id }).done((data) => {
            MaskHelper.HideBlockUI();
            processor.ShowModalInfo(config.resource.infoTitle, data.html);
        });
    }

    ChangeStatus(id) {
        var processor = this;
        var config = this._config;

        MaskHelper.ShowBlockUi();
        $.get(config.url.changeStatus, { id: id }).done((data) => {
            MaskHelper.HideBlockUI();
            processor.ShowModalChange(config.resource.changeStatusTitle, data.html, function () {
                processor.ChangeStatusSubmit();
                return false;
            });
        });
    }

    ChangeStatusSubmit() {
        var processor = this;
        this.HideModelErrors();
        var model = this.FindByName("change-status-form").serialize();

        MaskHelper.ShowBlockUi();
        $.ajax({ url: this._config.url.changeStatus, type: "POST", data: model }).done((data) => {
            MaskHelper.HideBlockUI();
            if (data.status) {
                bootbox.hideAll();
                CommonHelper.attention(processor._config.resource.operationComplete, processor._config.resource.changeStatusIsComplete);
            } else {
                processor.ShowModelErrors(data.errors, "change-status-form");
            }
        });
    }

    ChangeInverse(id) {
        var processor = this;
        var config = this._config;

        MaskHelper.ShowBlockUi();
        $.get(config.url.change, { id: id }).done((data) => {
            MaskHelper.HideBlockUI();
            processor.ShowModal(config.resource.changeTitle, data.html, function () {
                processor.ChangeInverseSubmit(this);
                return false;
            });
        });
    }

    ChangeInverseSubmit(btn) {
        $(btn).find("i").attr("class", "fa fa-spin fa-spinner");
        var processor = this;
        this.HideModelErrors();

        var model = this.FindByName("change-inverse-form").serialize();

        MaskHelper.ShowBlockUi();
        $.ajax({ url: this._config.url.change, type: "POST", data: model }).done((data) => {
            $(btn).find("i").attr("class", "fa fa-check");
            MaskHelper.HideBlockUI();
            if (data.status) {
                bootbox.hideAll();
                CommonHelper.attention(processor._config.resource.operationComplete, processor._config.resource.changeIsComplete);
            } else {
                processor.ShowModelErrors(data.errors, "change-inverse-form");
            }
        });
    }

    Start() {
        this.Bindings();
    }
}