﻿"use strict";
class VideoViewProcessor {
    constructor(config) {
        this._config = config;
    }

    // private
    FindByName(name) {
        return $(`[let="${name}"]`);
    }

    TryValue(object) {
        if (object == undefined || object === NaN || object.length === 0)
            return "";

        return object.val();
    }

    // private
    BindClick(name, callback) {
        this.FindByName(name).unbind("click").on("click", callback);
    }

    // private
    ShowModal(title, html, callback) {
        var config = this._config;
        bootbox.dialog({
            size: "large",
            title: title,
            message: html,
            backdrop: true,
            animate: true,
            onEscape: true,
            buttons: {
                save: {
                    label: `<i class="fa fa-check"></i> ${config.resource.save}`,
                    className: "btn btn-sm btn-success text-uppercase",
                    callback: callback
                },
                cancel: {
                    label: `<i class="fa fa-ban"></i> ${config.resource.cancel}`,
                    className: "btn btn-sm btn-danger text-uppercase",
                    callback: function () {
                        bootbox.hideAll();
                    }
                }
            }
        });
    }

    ShowModalInfo(title, html) {
        var config = this._config;
        bootbox.dialog({
            size: "large",
            title: title,
            message: html,
            backdrop: true,
            animate: true,
            onEscape: true,
            buttons: {
                cancel: {
                    label: `<i class="fa fa-close"></i> ${config.resource.close}`,
                    className: "btn btn-sm btn-danger text-uppercase",
                    callback: function () {
                        bootbox.hideAll();
                    }
                }
            }
        });
    }

    HideModelErrors() {
        $("[valid-for]").html("");
    }

    ShowModelErrors(errors, formName) {
        var form = this.FindByName(formName);
        $.each(errors, function (key) {
            form.find(`[valid-for='${key}']`).html(errors[key]);
        });
    }

    Bindings() {
        var processor = this;
        this.BindClick("delete-video", function () { processor.DeleteVideo($(this).attr("rid")) });
        this.BindClick("add-video", function () { processor.AddNew() });
        this.BindClick("change-video", function () { processor.ChangeVideo($(this).attr("rid")) });
    }

    RenderTable() {
        var processor = this;

        MaskHelper.ShowBlockUi();
        $.get(this._config.url.getAll).done((data) => {
            MaskHelper.HideBlockUI();
            processor.FindByName("video-table").html(data.html);
            processor.Bindings();
        });
    }

    ChangeVideo(id) {
        var processor = this;
        var config = this._config;

        MaskHelper.ShowBlockUi();
        $.get(config.url.change, { id: id }).done((data) => {
            MaskHelper.HideBlockUI();
            processor.ShowModal(config.resource.changeTitle, data.html, function () {
                processor.ChangeVideoSubmit(this);
                return false;
            });
        });
    }

    ChangeVideoSubmit(btn) {
        $(btn).find("i").attr("class", "fa fa-spin fa-spinner");
        var processor = this;
        this.HideModelErrors();

        var model = this.FindByName("change-video-form").serialize();

        MaskHelper.ShowBlockUi();
        $.ajax({ url: this._config.url.change, type: "POST", data: model  }).done((data) => {
            $(btn).find("i").attr("class", "fa fa-check");
            MaskHelper.HideBlockUI();
            if (data.status) {
                processor.RenderTable();
                bootbox.hideAll();
                CommonHelper.attention(processor._config.resource.operationComplete, processor._config.resource.changeIsComplete);
            } else {
                processor.ShowModelErrors(data.errors, "change-video-form");
            }
        });
    }


    AddNew() {
        var processor = this;
        var config = this._config;

        MaskHelper.ShowBlockUi();
        $.get(config.url.add).done((data) => {
            MaskHelper.HideBlockUI();
            processor.ShowModal(config.resource.addTitle, data.html, function () {
                processor.AddNewSubmit(this);
                return false;
            });
        });
    }

    AddNewSubmit(btn) {
        $(btn).find("i").attr("class", "fa fa-spin fa-spinner");
        var processor = this;
        this.HideModelErrors();
        var model = this.FindByName("add-video-form").serialize();
        
        MaskHelper.ShowBlockUi();
        $.ajax({ url: this._config.url.add, type: "POST", data: model  }).done((data) => {
            $(btn).find("i").attr("class", "fa fa-check");
            MaskHelper.HideBlockUI();
            if (data.status) {
                processor.RenderTable();
                bootbox.hideAll();
                CommonHelper.attention(processor._config.resource.operationComplete, processor._config.resource.addIsComplete);
            } else {
                processor.ShowModelErrors(data.errors, "add-video-form");
            }
        });
    }


    DeleteVideo(id) {
        var processor = this;
        bootbox.dialog({
            size: "small",
            message: processor._config.resource.deleteTitle,
            backdrop: true,
            animate: true,
            onEscape: true,
            buttons: {
                save: {
                    label: `<i class="fa fa-check"></i> ${processor._config.resource.yes}`,
                    className: "btn btn-sm btn-success text-uppercase",
                    callback: function () {
                        processor.DeleteVideoSubmit(id);
                    }
                },
                cancel: {
                    label: `<i class="fa fa-ban"></i> ${processor._config.resource.no}`,
                    className: "btn btn-sm btn-danger text-uppercase",
                    callback: function () {
                        bootbox.hideAll();
                    }
                }
            }
        });
    }

    DeleteVideoSubmit(id) {
        var processor = this;

        MaskHelper.ShowBlockUi();
        $.get(processor._config.url.delete, { id: id }).done((data) => {
            MaskHelper.HideBlockUI();
            if (data.status) {
                processor.RenderTable();
                CommonHelper.attention(processor._config.resource.operationComplete, processor._config.resource.deleteIsComplete);
            }
        });
    }

    Start() {
        this.RenderTable();
        this.Bindings();
    }
}