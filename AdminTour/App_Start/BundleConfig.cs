﻿using System.Web;
using System.Web.Optimization;

namespace AdminTour
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/registration").Include(
                      "~/Content/bower_components/jquery/dist/jquery.min.js",
                      "~/Content/bower_components/bootstrap/dist/js/bootstrap.min.js",
                      "~/Content/bower_components/bootstrap/dist/js/login-page-kboard.js",
                      "~/Content/plugins/iCheck/icheck.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/layout").Include(
                      "~/Content/bower_components/jquery/dist/jquery.min.js",
                      "~/Content/bower_components/jquery-ui/jquery-ui.min.js",
                      "~/Scripts/jquery.blockUI.js",
                      "~/Content/bower_components/bootstrap/dist/js/bootstrap.min.js",
                      "~/Content/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js",
                      "~/Content/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js",
                      "~/Content/bower_components/jquery-slimscroll/jquery.slimscroll.min.js",
                      "~/Scripts/jquery.gritter.js",
                      "~/Content/bower_components/fastclick/lib/fastclick.js",
                      "~/Content/dist/js/adminlte.min.js",
                      "~/Content/dist/js/pages/dashboard.js",
                      "~/Content/dist/js/demo.js",
                      "~/Scripts/bootbox.js",
                      "~/Scripts/MaskHelper.js",
                      "~/Scripts/AjaxHelper.js",
                      "~/Scripts/CommonHelper.js",
                      "~/Scripts/moment.js",
                      "~/Scripts/moment-with-locales.js",
                      "~/Scripts/bootstrap-datetimepicker.js",
                      "~/Scripts/controls/paginator.js",
                      "~/Scripts/jquery.cookie-1.4.1.min.js",
                      "~/Scripts/jquery.cookie.min.js",
                      "~/Scripts/CultureInfo.js",
                      "~/Scripts/select2/select2.min.js",
                      "~/Scripts/controls/file.upload.js",
                      "~/Scripts/controls/enum.dropdown.list.js"));






            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap/dist/css/bootstrap.css",
                      "~/Content/bootstrap/dist/css/bootstrap-theme.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/registration").Include(
                      "~/Content/bower_components/bootstrap/dist/css/bootstrap.min.css",
                      "~/Content/bower_components/bootstrap/dist/css/keyboard.css",
                      "~/Content/bower_components/font-awesome/css/font-awesome.min.css",
                      "~/Content/bower_components/Ionicons/css/ionicons.min.css",
                      "~/Content/dist/css/AdminLTE.css",
                      "~/Content/plugins/iCheck/square/blue.css"));


            bundles.Add(new StyleBundle("~/Content/layout").Include(
                      "~/Content/bower_components/bootstrap/dist/css/bootstrap.min.css",
                      "~/Content/bower_components/font-awesome/css/font-awesome.min.css",
                      "~/Content/bower_components/Ionicons/css/ionicons.min.css",
                      "~/Content/jquery.gritter.css",
                      "~/Content/dist/css/AdminLTE.css",
                      "~/Content/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css",
                      "~/Content/dist/css/skins/skin-black.css",
                      "~/Content/bootstrap-datetimepicker.css",
                      "~/Content/select2.css",
                      "~/Content/awesome-bootstrap-checkbox.css"));
        }
    }
}
