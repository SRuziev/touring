﻿var shiftK = "DISABLE";
var langK = "ENG";
var toInputId = "";
var toShowId = "";
var inputString = "";
var DataResources = null;

function onLoadKboard(hide, show, resources) {
    DataResources = resources;
    generateKboard("DISABLE", "ENG");
    toInputId = hide;
    toShowId = show;
}

function generateKboard(shift, lang) {
    if (shift === "DISABLE") {
        if (lang === "ENG") $('#keyboard').html(norm_eng_key());
        if (lang === "RU") $('#keyboard').html(norm_ru_key());
    }

    if (shift === "ACTIVE") {
        if (lang === "ENG") $('#keyboard').html(caps_eng_key());
        if (lang === "RU") $('#keyboard').html(caps_ru_key());
    }

}
function norm_eng_key() {
    return addK("1") + addK("2") + addK("3") + addK("4") + addK("5") + addK("6") + addK("7") + addK("8") + addK("9") + addK("0") + addK("-") + addK("=") + addL("DELETE", "<i class='fa fa-long-arrow-left'></i> " + DataResources.delete, 91, "")
        + addK("q") + addK("w") + addK("e") + addK("r") + addK("t") + addK("y") + addK("u") + addK("i") + addK("o") + addK("p") + addK("[") + addK("]") + addL("CLEAR", "<i class='fa fa-eraser'></i> " + DataResources.clear, 91, "")
        + addK("a") + addK("s") + addK("d") + addK("f") + addK("g") + addK("h") + addK("j") + addK("k") + addK("l") + addK(";") + addK("/") + addL("TO_RU", "ENG", 124, "")
        + addK("z") + addK("x") + addK("c") + addK("v") + addK("b") + addK("n") + addK("m") + addK(",") + addK(".") + addK("?") + addL("ACTIVE_SHIFT", "<i class='fa fa-arrow-up'></i> Shift", 157, "")
        + addClearFix();
}
function caps_eng_key() {
    return addK("!") + addK("@") + addK("#") + addK("$") + addK("%") + addK("^") + addK("&") + addK(";") + addK("(") + addK(")") + addK("_") + addK("+") + addL("DELETE", "<i class='fa fa-long-arrow-left'></i> " + DataResources.delete, 91, "")
        + addK("Q") + addK("W") + addK("E") + addK("R") + addK("T") + addK("Y") + addK("U") + addK("I") + addK("O") + addK("P") + addK("{") + addK("}") + addL("CLEAR", "<i class='fa fa-eraser'></i> " + DataResources.clear, 91, "")
        + addK("A") + addK("S") + addK("D") + addK("F") + addK("G") + addK("H") + addK("J") + addK("K") + addK("L") + addK(":") + addK("\\") + addL("TO_RU", "ENG", 124, "")
        + addK("Z") + addK("X") + addK("C") + addK("V") + addK("B") + addK("N") + addK("M") + addK("<") + addK(">") + addK("?") + addL("DISABLE_SHIFT", "<i class='fa fa-arrow-up'></i> Shift", 157, "active")
        + addClearFix();
}
function norm_ru_key() {
    return addK("1") + addK("2") + addK("3") + addK("4") + addK("5") + addK("6") + addK("7") + addK("8") + addK("9") + addK("0") + addK("-") + addK("=") + addL("DELETE", "<i class='fa fa-long-arrow-left'></i> " + DataResources.delete, 91, "")
        + addK("й") + addK("ц") + addK("у") + addK("к") + addK("е") + addK("н") + addK("г") + addK("ш") + addK("щ") + addK("з") + addK("х") + addK("ъ") + addL("CLEAR", "<i class='fa fa-eraser'></i> " + DataResources.clear, 91, "")
        + addK("ф") + addK("ы") + addK("в") + addK("а") + addK("п") + addK("р") + addK("о") + addK("л") + addK("д") + addK("ж") + addK("э") + addL("TO_ENG", "РУС", 124, "")
        + addK("я") + addK("ч") + addK("с") + addK("м") + addK("и") + addK("т") + addK("ь") + addK("б") + addK("ю") + addK(".") + addK("?") + addL("ACTIVE_SHIFT", "<i class='fa fa-arrow-up'></i> Shift", 124, "")
        + addClearFix();
}
function caps_ru_key() {
    return addK("!") + addK("@") + addK("#") + addK("$") + addK("%") + addK("^") + addK("&") + addK(";") + addK("(") + addK(")") + addK("_") + addK("+") + addL("DELETE", "<i class='fa fa-long-arrow-left'></i> " + DataResources.delete, 91, "")
        + addK("Й") + addK("Ц") + addK("У") + addK("К") + addK("Е") + addK("Н") + addK("Г") + addK("Ш") + addK("Щ") + addK("З") + addK("Х") + addK("Ъ") + addL("CLEAR", "<i class='fa fa-eraser'></i> " + DataResources.clear, 91, "")
        + addK("Ф") + addK("Ы") + addK("В") + addK("А") + addK("П") + addK("Р") + addK("О") + addK("Л") + addK("Д") + addK("Ж") + addK("Э") + addL("TO_ENG", "РУС", 124, "")
        + addK("Я") + addK("Ч") + addK("С") + addK("М") + addK("И") + addK("Т") + addK("Ь") + addK("Б") + addK("Ю") + addK(",") + addK("/") + addL("DISABLE_SHIFT", "<i class='fa fa-arrow-up'></i> Shift", 124, "active")
        + addClearFix();
}
function addK(symbol) {
    return "<div class='button simple' unselectable='on' onclick='pressSymbol(\"" + symbol + "\")'>" + symbol + "</div>";
}
function addL(id, word, width, uClass) {
    return "<div id='" + id + "' class='button large " + uClass + "' style='width: " + width + "px' unselectable='on' onclick='pressSymbol(this.id)'>" + word + "</div>";
}
function addClearFix() {
    return "<div class='clearfix'></div>";
}
function pressSymbol(key) {
    switch (key) {
        case "DELETE":
            {
                if (inputString.length > 0) {
                    inputString = inputString.substring(0, inputString.length - 1);
                    $("#" + toInputId + "").val(inputString);
                    $("#" + toShowId + "").val(inputString);
                }
                break;
            }
        case "CLEAR":
            {
                inputString = "";
                $("#" + toInputId + "").val(inputString);
                $("#" + toShowId + "").val(inputString);
                break;
            }
        case "TO_RU":
            {
                langK = "RU";
                generateKboard(shiftK, langK);
                break;
            }
        case "TO_ENG":
            {
                langK = "ENG";
                generateKboard(shiftK, langK);
                break;
            }
        case "ACTIVE_SHIFT":
            {
                shiftK = "ACTIVE";
                generateKboard(shiftK, langK);
                break;
            }
        case "DISABLE_SHIFT":
            {
                shiftK = "DISABLE";
                generateKboard(shiftK, langK);
                break;
            }
        default:
            {
                inputString += key;
                $("#" + toInputId + "").val(inputString);
                $("#" + toShowId + "").val(inputString);
                break;
            }
    }
}