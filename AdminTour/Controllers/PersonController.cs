﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTour.Processors;
using KIT.Helpers;
using KIT.Models;
using KIT.ModelsVm;

namespace AdminTour.Controllers
{
    public class PersonController : AdminController
    {
        private readonly PersonProcessor _personProcessor;

        public PersonController()
        {
            _personProcessor = new PersonProcessor(CommonRepository);
        }

        [HttpGet]
        public ActionResult MyProfile()
        {
            var person = AccessContext.GetAccount;
            return Json(new { html = this.PartialViewToString("_changeProfileForm", _personProcessor.MyProfile(person.Id)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ChangeProfile(AdminAccountVm adminAccountVm)
        {
            var account = CommonRepository.SingleOrDefault<AdminAccount>(w => w.Id == adminAccountVm.Id);
            adminAccountVm.Password = account.Password;
            var errors = new Dictionary<string, string>();
            if (!_personProcessor.IsValidProfile(errors, adminAccountVm))
                return Json(new { status = false, errors }, JsonRequestBehavior.AllowGet);

            _personProcessor.ChangeProfile(adminAccountVm);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }
    }
}