﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using AdminTour.Models;
using KIT.Enums;
using KIT.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Hash = KIT.Helpers.Hash;


namespace AdminTour.Controllers
{
    
    public class AccountController :AdminController
    {
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost, AllowAnonymous, ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            var recaptcha = Request.Params["g-recaptcha-response"];
            if (!ValidateReCaptcha(recaptcha))
            {
                return RedirectToAction("Login", "Account");
            }
            var auth = CommonRepository.SingleOrDefault<AdminAccount>(w => w.Login == model.Login);
            if (auth == null)
            {
                ModelState.AddModelError("Login", "Не верный логин");
                return View(model);
            }

            if (auth.Password != Hash.GetHash(model.Password, HashType.MD5))
            {
                ModelState.AddModelError("Password", "Не верный пароль");
                return View(model);
            }

            if (!auth.IsActiv)
            {
                ModelState.AddModelError("Login", "Ваш логин заблокирован");
                return View(model);
            }

            FormsAuthentication.SetAuthCookie(auth.Login, false);
            //return RedirectToAction("Index", "Home");
            return RedirectToLocal(returnUrl);
        }

        [HttpGet]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        private static bool ValidateReCaptcha(string mainresponse)
        {
            try
            {
                if (WebConfigurationManager.AppSettings["RecaptchaValidate"] == "true")
                {
                    var privatekey = WebConfigurationManager.AppSettings["RecaptchaKey"];
                    var req = (HttpWebRequest)WebRequest.Create("https://www.google.com/recaptcha/api/siteverify?secret=" + privatekey + "&response=" + mainresponse);
                    var response = req.GetResponse();
                    using (var readStream = new StreamReader(response.GetResponseStream()))
                    {
                        var jsonResponse = readStream.ReadToEnd();
                        var reCaptcha = JsonConvert.DeserializeObject<ReCaptcha>(jsonResponse);
                        return reCaptcha.Success;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public class ReCaptcha
        {
            public bool Success { get; set; }
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}