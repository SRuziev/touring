﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using KIT.Helpers;
using KIT.Models;
using KIT.Repositories;

namespace AdminTour.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        protected readonly ICommonRepository CommonRepository;

        public AdminController()
        {
            CommonRepository = new CommonRepository();
        }

        protected override void Dispose(bool disposing)
        {
            CommonRepository.Dispose();
            base.Dispose(disposing);
        }

        protected AdminAccount GetUserInfo
        {
            get
            {
                try
                {
                    return AccessContext.GetAccount;
                }
                catch (Exception)
                {
                    FormsAuthentication.SignOut();
                    return new AdminAccount();
                }
            }
        }
    }
}