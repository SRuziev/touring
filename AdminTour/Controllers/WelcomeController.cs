﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTour.Processors;
using KIT.Helpers;
using KIT.Models;

namespace AdminTour.Controllers
{
    public class WelcomeController : AdminController
    {

        private readonly WelcomeProcessor _welcomeProcessor;
        public WelcomeController()
        {
            _welcomeProcessor = new WelcomeProcessor(CommonRepository);
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Приветствие";
            var welcome = CommonRepository.Entities<Welcome>().SingleOrDefault();
            return View(welcome);
        }

        [HttpGet]
        public ActionResult AllText()
        {
            return Json(new { html = this.PartialViewToString("_allText",_welcomeProcessor.GetText()) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ChangeText(long id)
        {
            return Json(new { html = this.PartialViewToString("_changeText", _welcomeProcessor.GetWelcome(id)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ChangeText(Welcome welcome)
        {
            Dictionary<string, string> errors;
            if (!_welcomeProcessor.IsValidChange(welcome, out errors))
                return Json(new { status = false, errors }, JsonRequestBehavior.AllowGet);

            _welcomeProcessor.ChangeText(welcome);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }
    }
}