﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTour.Processors;

namespace AdminTour.Controllers
{
    public class StatisticsController : AdminController
    {
        private readonly StatisticsProcessor _statisticsProcessor;
        public StatisticsController()
        {
            _statisticsProcessor = new StatisticsProcessor(CommonRepository);
        }
        // GET: Statistics
        public ActionResult Index()
        {
            return View(_statisticsProcessor.GetStatisticsVm());
        }
    }
}