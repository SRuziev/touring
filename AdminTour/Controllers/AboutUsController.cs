﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTour.Processors;
using KIT.Helpers;
using KIT.Models;
using KIT.ModelsVm;

namespace AdminTour.Controllers
{
    public class AboutUsController : AdminController
    {
        private readonly AboutUsProcessor _aboutUsProcessor;
        public AboutUsController()
        {
            _aboutUsProcessor = new AboutUsProcessor(CommonRepository);
        }
        // GET: AboutUs
        public ActionResult Index()
        {
            var about = CommonRepository.Entities<AboutUs>().SingleOrDefault();
            return View(about);
        }

        [HttpGet]
        public ActionResult AllText()
        {
            return Json(new { html = this.PartialViewToString("_allText", _aboutUsProcessor.GetText()) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ChangeText(long id)
        {
            return Json(new { html = this.PartialViewToString("_changeText", _aboutUsProcessor.GetAboutUs(id)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ChangeText(AboutUs aboutUs)
        {
            Dictionary<string, string> errors;
            if (!_aboutUsProcessor.IsValidChange(aboutUs, out errors))
                return Json(new { status = false, errors }, JsonRequestBehavior.AllowGet);

            _aboutUsProcessor.ChangeText(aboutUs);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }
    }
}