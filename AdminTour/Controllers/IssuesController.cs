﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTour.Processors;
using KIT.Helpers;
using KIT.ModelsVm;

namespace AdminTour.Controllers
{
    public class IssuesController : AdminController
    {
        private readonly IssuesProcessor _issuesProcessor;

        public IssuesController()
        {
            _issuesProcessor = new IssuesProcessor(CommonRepository);
        }

        // GET: Issues
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AllIssues()
        {
            return Json(new { html = this.PartialViewToString("_allIssues", _issuesProcessor.AllIssuesClient()) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddIssues()
        {
            return Json(new { html = this.PartialViewToString("_addIssues", new IssueTourVm()) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult AddIssues(IssueTourVm issuesClientVm)
        {
            Dictionary<string, string> errors;
            if (!_issuesProcessor.IsValidIssues(issuesClientVm, out errors))
                return Json(new { status = false, errors }, JsonRequestBehavior.AllowGet);

            _issuesProcessor.AddIssues(issuesClientVm);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeleteIssues(long id)
        {
            _issuesProcessor.DeleteIssues(id);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult IssuesInfo(long? id)
        {
            return Json(new { html = this.PartialViewToString("_issuesInfo", _issuesProcessor.GetIssuesClientVm(id)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ChangeIssues(long? id)
        {
            return Json(new { html = this.PartialViewToString("_changeIssues", _issuesProcessor.GetIssuesClientVm(id)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ChangeIssues(IssueTourVm issuesClientVm)
        {
            Dictionary<string, string> errors;
            if (!_issuesProcessor.IsValidIssues(issuesClientVm, out errors))
                return Json(new { status = false, errors }, JsonRequestBehavior.AllowGet);

            _issuesProcessor.ChangeIssues(issuesClientVm);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }
    }
}