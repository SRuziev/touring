﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTour.Processors;
using KIT.Helpers;
using KIT.Models;
using KIT.ModelsVm;

namespace AdminTour.Controllers
{
    public class SecurityController : AdminController
    {
        private readonly SecurityProcessor _securityProcessor;

        public SecurityController()
        {
            _securityProcessor = new SecurityProcessor(CommonRepository);
        }

        // GET: Security
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AllIp()
        {
            return Json(new { html = this.PartialViewToString("_allIp", _securityProcessor.AllIp()) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddIp()
        {
            return Json(new { html = this.PartialViewToString("_addIp", new SecurityVm()) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult AddIp(SecurityVm securityVm)
        {
            Dictionary<string, string> errors;
            if (!_securityProcessor.IsValidProfile(securityVm,out errors))
                return Json(new { status = false, errors }, JsonRequestBehavior.AllowGet);

            _securityProcessor.AddIp(securityVm);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeleteIp(long id)
        {
            _securityProcessor.DeleteIp(id);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }
    }
}