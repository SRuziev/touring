﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTour.Processors;
using KIT.Helpers;
using KIT.ModelsVm;

namespace AdminTour.Controllers
{
    public class ContactsController : AdminController
    {

        private readonly ContactsProcessor _contactsProcessor;

        public ContactsController()
        {
            _contactsProcessor = new ContactsProcessor(CommonRepository);
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AllContacts()
        {
            return Json(new { html = this.PartialViewToString("_allContact", _contactsProcessor.GetContacts()) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ChangeContact(long? id)
        {
            return Json(new { html = this.PartialViewToString("_changeContact", _contactsProcessor.GetContactsVm(id)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ChangeContact(ContactsVm contactsVm)
        {
            Dictionary<string, string> errors;
            if (!_contactsProcessor.IsValidContacts(contactsVm, out errors))
                return Json(new { status = false, errors }, JsonRequestBehavior.AllowGet);

            _contactsProcessor.ChangeContacts(contactsVm);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

    }
}