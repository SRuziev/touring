﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTour.Processors;
using KIT.Helpers;
using KIT.Models.BaseModels;
using KIT.ModelsVm;

namespace AdminTour.Controllers
{
    public class InverseController : AdminController
    {
        //private readonly PaymentProcessor _paymentProcessor;
        private readonly InverseListProcessor _inverseListProcessor;
        private readonly InverseProcessor _inverseProcessor;

        public InverseController()
        {
           // _paymentProcessor = new PaymentProcessor(CommonRepository);
            _inverseListProcessor = new InverseListProcessor(CommonRepository);
            _inverseProcessor = new InverseProcessor(CommonRepository);
        }
        // GET: Inverse
        public ActionResult Index()
        {
            return View(_inverseListProcessor.ToFilteredResult);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Index(ListViewModel<InverseContactVm, InverseFilter> viewModel)
        {
            
            _inverseListProcessor.Initialize(viewModel);

            return View(_inverseListProcessor.ToFilteredResult);
        }

        [HttpGet]
        public ActionResult ChangeStatus(long id)
        {
            return Json(new { html = this.PartialViewToString("_inverse", _inverseProcessor.InverseStatus(id)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ChangeStatus(InverseStatusVm inverseStatusVm)
        {
            _inverseProcessor.ChangeStatus(inverseStatusVm);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }
    }
}