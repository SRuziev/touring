﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTour.Processors;
using KIT.Helpers;
using KIT.Models.BaseModels;
using KIT.ModelsVm;

namespace AdminTour.Controllers
{
    public class InverseTourController : AdminController
    {
        private readonly InverseTourListProcessor _inverseTourListProcessor;
        private readonly InverseTourProcessor _inverseTourProcessor;

        public InverseTourController()
        {
            _inverseTourListProcessor = new InverseTourListProcessor(CommonRepository);
            _inverseTourProcessor = new InverseTourProcessor(CommonRepository);
        }


        // GET: InverseTour
        public ActionResult Index()
        {
            return View(_inverseTourListProcessor.ToFilteredResult);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Index(ListViewModel<InverseTourVm, InverseTourFilter> viewModel)
        {
            _inverseTourListProcessor.Initialize(viewModel);
            return View(_inverseTourListProcessor.ToFilteredResult);
        }

        [HttpGet]
        public ActionResult ChangeStatus(long id)
        {
            return Json(new { html = this.PartialViewToString("_inverse", _inverseTourProcessor.GetInverseStatus(id)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ChangeStatus(InverseStatusVm inverseStatusVm)
        {
            _inverseTourProcessor.ChangeStatus(inverseStatusVm);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult IssuesInfo(long id)
        {
            return Json(new { html = this.PartialViewToString("_infoInverse", _inverseTourProcessor.GetInfoInverseTourVm(id)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ChangeInverse(long id)
        {
            return Json(new { html = this.PartialViewToString("_changeInverseTour", _inverseTourProcessor.GetInfoInverseTourVm(id)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ChangeInverse(InverseInfoTourVm inverseInfoTourVm)
        {
            Dictionary<string, string> errors;
            if (!_inverseTourProcessor.IsValidChangeInverse(inverseInfoTourVm, out errors))
                return Json(new { status = false, errors }, JsonRequestBehavior.AllowGet);

            _inverseTourProcessor.ChangeInverseTour(inverseInfoTourVm);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

    }
}
