﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTour.Processors;
using KIT.Helpers;
using KIT.Models;
using KIT.ModelsVm;

namespace AdminTour.Controllers
{
    public class VideoController : AdminController
    {
         private readonly VideoProcessor _videoProcessor;
        public VideoController()
        {
            _videoProcessor = new VideoProcessor(CommonRepository);
        }

        // GET: Video
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AllVideo()
        {
            return Json(new { html = this.PartialViewToString("_allVideo", _videoProcessor.GetAll()) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddVideo()
        {
            return Json(new { html = this.PartialViewToString("_addVideo", new VideoVm()) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult AddVideo(VideoVm videoVm)
        {
            Dictionary<string, string> errors;
            if (!_videoProcessor.IsValid(videoVm, out errors))
                return Json(new { status = false, errors }, JsonRequestBehavior.AllowGet);

            _videoProcessor.AddVideo(videoVm);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeleteVideo(long id)
        {
            _videoProcessor.Delete(id);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ChangeVideo(long id)
        {
            return Json(new { html = this.PartialViewToString("_changeVideo", _videoProcessor.GetVideo(id)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ChangeVideo(Video video)
        {
            Dictionary<string, string> errors;
            if (!_videoProcessor.IsValid(video, out errors))
                return Json(new { status = false, errors }, JsonRequestBehavior.AllowGet);

            _videoProcessor.ChangeVideo(video);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

    }
}