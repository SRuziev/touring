﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTour.Processors;
using KIT.Helpers;
using KIT.ModelsVm;

namespace AdminTour.Controllers
{
    public class TourController : AdminController
    {
        private readonly TourProcessor _tourProcessor;

        public TourController()
        {
            _tourProcessor = new TourProcessor(CommonRepository);
        }
        // GET: Tour
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AllTour()
        {
            return Json(new { html = this.PartialViewToString("_allTour", _tourProcessor.AllTour()) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddTour()
        {
            return Json(new { html = this.PartialViewToString("_addTour", new TourVm()) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddTour(TourVm tourVm)
        {
            Dictionary<string, string> errors;
            if (!_tourProcessor.IsValidAddTour(tourVm, out errors))
                return Json(new { status = false, errors }, JsonRequestBehavior.AllowGet);

            _tourProcessor.AddTour(tourVm);
            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeleteTour(long id)
        {
            _tourProcessor.LogicDelete(id);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult TourInfo(long? id)
        {
            return Json(new { html = this.PartialViewToString("_tourInfo", _tourProcessor.TourInfo(id)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ChangeTour(long? id)
        {
            return Json(new { html = this.PartialViewToString("_changeTour", _tourProcessor.TourInfo(id)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ChangeTour(TourVm tourVm)
        {
            Dictionary<string, string> errors;
            if (!_tourProcessor.IsValidChangeTour(tourVm, out errors))
                return Json(new { status = false, errors }, JsonRequestBehavior.AllowGet);

            _tourProcessor.ChangeTour(tourVm);
            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }
    }
    }
